<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatenteAduanalRepository")
 */
class PatenteAduanal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $patente;

    /**
     * @ORM\Column(type="date")
     */
    private $inicioVigencia;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NumPedimientoAduana", mappedBy="patente", orphanRemoval=true)
     */
    private $numsPedimiento;

    public function __construct()
    {
        $this->numsPedimiento = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->patente;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPatente(): ?string
    {
        return $this->patente;
    }

    public function setPatente(string $patente): self
    {
        $this->patente = $patente;

        return $this;
    }

    public function getInicioVigencia(): ?\DateTimeInterface
    {
        return $this->inicioVigencia;
    }

    public function setInicioVigencia(\DateTimeInterface $inicioVigencia): self
    {
        $this->inicioVigencia = $inicioVigencia;

        return $this;
    }

    /**
     * @return Collection|NumPedimientoAduana[]
     */
    public function getNumsPedimiento(): Collection
    {
        return $this->numsPedimiento;
    }

    public function addNumsPedimiento(NumPedimientoAduana $numsPedimiento): self
    {
        if (!$this->numsPedimiento->contains($numsPedimiento)) {
            $this->numsPedimiento[] = $numsPedimiento;
            $numsPedimiento->setPatente($this);
        }

        return $this;
    }

    public function removeNumsPedimiento(NumPedimientoAduana $numsPedimiento): self
    {
        if ($this->numsPedimiento->contains($numsPedimiento)) {
            $this->numsPedimiento->removeElement($numsPedimiento);
            // set the owning side to null (unless already changed)
            if ($numsPedimiento->getPatente() === $this) {
                $numsPedimiento->setPatente(null);
            }
        }

        return $this;
    }
}
