<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaisRepository")
 */
class Pais
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $cve;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $formatoCP;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $formatoRegTributario;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $valorRegTributario;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $agrupaciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Receptor", mappedBy="recidenciaFiscal")
     */
    private $receptores;

    public function __construct()
    {
        $this->receptores = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->cve;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCve(): ?string
    {
        return $this->cve;
    }

    public function setCve(string $cve): self
    {
        $this->cve = $cve;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getFormatoCP(): ?string
    {
        return $this->formatoCP;
    }

    public function setFormatoCP(?string $formatoCP): self
    {
        $this->formatoCP = $formatoCP;

        return $this;
    }

    public function getFormatoRegTributario(): ?string
    {
        return $this->formatoRegTributario;
    }

    public function setFormatoRegTributario(?string $formatoRegTributario): self
    {
        $this->formatoRegTributario = $formatoRegTributario;

        return $this;
    }

    public function getValorRegTributario(): ?string
    {
        return $this->valorRegTributario;
    }

    public function setValorRegTributario(?string $valorRegTributario): self
    {
        $this->valorRegTributario = $valorRegTributario;

        return $this;
    }

    public function getAgrupaciones(): ?string
    {
        return $this->agrupaciones;
    }

    public function setAgrupaciones(?string $agrupaciones): self
    {
        $this->agrupaciones = $agrupaciones;

        return $this;
    }

    /**
     * @return Collection|Receptor[]
     */
    public function getReceptores(): Collection
    {
        return $this->receptores;
    }

    public function addReceptore(Receptor $receptore): self
    {
        if (!$this->receptores->contains($receptore)) {
            $this->receptores[] = $receptore;
            $receptore->setRecidenciaFiscal($this);
        }

        return $this;
    }

    public function removeReceptore(Receptor $receptore): self
    {
        if ($this->receptores->contains($receptore)) {
            $this->receptores->removeElement($receptore);
            // set the owning side to null (unless already changed)
            if ($receptore->getRecidenciaFiscal() === $this) {
                $receptore->setRecidenciaFiscal(null);
            }
        }

        return $this;
    }
}
