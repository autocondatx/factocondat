<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConceptoRepository")
 */
class Concepto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConceptoRetenciones", mappedBy="concepto")
     */
    private $retenciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConceptoTraslados", mappedBy="concepto")
     */
    private $traslados;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConceptoInfoAduanera", mappedBy="concepto")
     */
    private $infoAduanera;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cuentaPredialNum;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complementos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $partes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClaveProdServ", inversedBy="conceptos")
     */
    private $claveProdServ;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $noIdentificacion;

    /**
     * @ORM\Column(type="float")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClaveUnidad", inversedBy="conceptos")
     */
    private $claveUnidad;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $unidad;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="float")
     */
    private $valorUnitario;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $descuento;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Comprobante", inversedBy="conceptos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $comprobante;

    public function __construct()
    {
        $this->retenciones = new ArrayCollection();
        $this->traslados = new ArrayCollection();
        $this->infoAduanera = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Collection|ConceptoRetenciones[]
     */
    public function getRetenciones(): Collection
    {
        return $this->retenciones;
    }

    public function addRetencione(ConceptoRetenciones $retencione): self
    {
        if (!$this->retenciones->contains($retencione)) {
            $this->retenciones[] = $retencione;
            $retencione->setConcepto($this);
        }

        return $this;
    }

    public function removeRetencione(ConceptoRetenciones $retencione): self
    {
        if ($this->retenciones->contains($retencione)) {
            $this->retenciones->removeElement($retencione);
            // set the owning side to null (unless already changed)
            if ($retencione->getConcepto() === $this) {
                $retencione->setConcepto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ConceptoTraslados[]
     */
    public function getTraslados(): Collection
    {
        return $this->traslados;
    }

    public function addTraslado(ConceptoTraslados $traslado): self
    {
        if (!$this->traslados->contains($traslado)) {
            $this->traslados[] = $traslado;
            $traslado->setConcepto($this);
        }

        return $this;
    }

    public function removeTraslado(ConceptoTraslados $traslado): self
    {
        if ($this->traslados->contains($traslado)) {
            $this->traslados->removeElement($traslado);
            // set the owning side to null (unless already changed)
            if ($traslado->getConcepto() === $this) {
                $traslado->setConcepto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ConceptoInfoAduanera[]
     */
    public function getInfoAduanera(): Collection
    {
        return $this->infoAduanera;
    }

    public function addInfoAduanera(ConceptoInfoAduanera $infoAduanera): self
    {
        if (!$this->infoAduanera->contains($infoAduanera)) {
            $this->infoAduanera[] = $infoAduanera;
            $infoAduanera->setConcepto($this);
        }

        return $this;
    }

    public function removeInfoAduanera(ConceptoInfoAduanera $infoAduanera): self
    {
        if ($this->infoAduanera->contains($infoAduanera)) {
            $this->infoAduanera->removeElement($infoAduanera);
            // set the owning side to null (unless already changed)
            if ($infoAduanera->getConcepto() === $this) {
                $infoAduanera->setConcepto(null);
            }
        }

        return $this;
    }

    public function getCuentaPredialNum(): ?string
    {
        return $this->cuentaPredialNum;
    }

    public function setCuentaPredialNum(?string $cuentaPredialNum): self
    {
        $this->cuentaPredialNum = $cuentaPredialNum;

        return $this;
    }

    public function getComplementos(): ?string
    {
        return $this->complementos;
    }

    public function setComplementos(?string $complementos): self
    {
        $this->complementos = $complementos;

        return $this;
    }

    public function getPartes(): ?string
    {
        return $this->partes;
    }

    public function setPartes(?string $partes): self
    {
        $this->partes = $partes;

        return $this;
    }

    public function getClaveProdServ(): ?ClaveProdServ
    {
        return $this->claveProdServ;
    }

    public function setClaveProdServ(?ClaveProdServ $claveProdServ): self
    {
        $this->claveProdServ = $claveProdServ;

        return $this;
    }

    public function getNoIdentificacion(): ?string
    {
        return $this->noIdentificacion;
    }

    public function setNoIdentificacion(?string $noIdentificacion): self
    {
        $this->noIdentificacion = $noIdentificacion;

        return $this;
    }

    public function getCantidad(): ?float
    {
        return $this->cantidad;
    }

    public function setCantidad(float $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getClaveUnidad(): ?ClaveUnidad
    {
        return $this->claveUnidad;
    }

    public function setClaveUnidad(?ClaveUnidad $claveUnidad): self
    {
        $this->claveUnidad = $claveUnidad;

        return $this;
    }

    public function getUnidad(): ?string
    {
        return $this->unidad;
    }

    public function setUnidad(?string $unidad): self
    {
        $this->unidad = $unidad;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getValorUnitario(): ?float
    {
        return $this->valorUnitario;
    }

    public function setValorUnitario(float $valorUnitario): self
    {
        $this->valorUnitario = $valorUnitario;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getDescuento(): ?float
    {
        return $this->descuento;
    }

    public function setDescuento(?float $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getComprobante(): ?Comprobante
    {
        return $this->comprobante;
    }

    public function setComprobante(?Comprobante $comprobante): self
    {
        $this->comprobante = $comprobante;

        return $this;
    }
}
