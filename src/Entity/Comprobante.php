<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ComprobanteRepository")
 */
class Comprobante
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CfdiRelacionados", mappedBy="comprobante")
     */
    private $cfdiRelacionados;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Emisor", inversedBy="comprobantes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $emisor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Receptor", inversedBy="comprobantes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $receptor;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Concepto", mappedBy="comprobante", orphanRemoval=true, cascade={"persist"})
     */
    private $conceptos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Retencion", mappedBy="comprobante", orphanRemoval=true)
     */
    private $retenciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Traslado", mappedBy="comprobante", orphanRemoval=true)
     */
    private $traslados;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalImpuestosRetenidos;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalImpuestosTrasladados;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complemento;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adenda;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $serie;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $folio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sello;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FormaPago", inversedBy="comprobantes")
     */
    private $formaPago;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $noCertificado;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $certificado;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $condicionesPago;

    /**
     * @ORM\Column(type="float")
     */
    private $subTotal;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $descuento;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Moneda", inversedBy="comprobantes")
     */
    private $moneda;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tipoCambio;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoComprobante", inversedBy="comprobantes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipoComprobante;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MetodoPago", inversedBy="comprobantes")
     */
    private $metodoPago;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CodigoPostal", inversedBy="comprobantes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lugarExpedicion;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $confirmacion;

    public function __construct()
    {
        $this->retenciones = new ArrayCollection();
        $this->traslados = new ArrayCollection();
        $this->conceptos = new ArrayCollection();
        $this->cfdiRelacionados = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmisor(): ?Emisor
    {
        return $this->emisor;
    }

    public function setEmisor(?Emisor $emisor): self
    {
        $this->emisor = $emisor;

        return $this;
    }

    public function getReceptor(): ?Receptor
    {
        return $this->receptor;
    }

    public function setReceptor(?Receptor $receptor): self
    {
        $this->receptor = $receptor;

        return $this;
    }

    /**
     * @return Collection|Retencion[]
     */
    public function getRetenciones(): Collection
    {
        return $this->retenciones;
    }

    public function addRetencione(Retencion $retencione): self
    {
        if (!$this->retenciones->contains($retencione)) {
            $this->retenciones[] = $retencione;
            $retencione->setComprobante($this);
        }

        return $this;
    }

    public function removeRetencione(Retencion $retencione): self
    {
        if ($this->retenciones->contains($retencione)) {
            $this->retenciones->removeElement($retencione);
            // set the owning side to null (unless already changed)
            if ($retencione->getComprobante() === $this) {
                $retencione->setComprobante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Traslado[]
     */
    public function getTraslados(): Collection
    {
        return $this->traslados;
    }

    public function addTraslado(Traslado $traslado): self
    {
        if (!$this->traslados->contains($traslado)) {
            $this->traslados[] = $traslado;
            $traslado->setComprobante($this);
        }

        return $this;
    }

    public function removeTraslado(Traslado $traslado): self
    {
        if ($this->traslados->contains($traslado)) {
            $this->traslados->removeElement($traslado);
            // set the owning side to null (unless already changed)
            if ($traslado->getComprobante() === $this) {
                $traslado->setComprobante(null);
            }
        }

        return $this;
    }

    public function getTotalImpuestosRetenidos(): ?float
    {
        return $this->totalImpuestosRetenidos;
    }

    public function setTotalImpuestosRetenidos(?float $totalImpuestosRetenidos): self
    {
        $this->totalImpuestosRetenidos = $totalImpuestosRetenidos;

        return $this;
    }

    public function getTotalImpuestosTrasladados(): ?float
    {
        return $this->totalImpuestosTrasladados;
    }

    public function setTotalImpuestosTrasladados(?float $totalImpuestosTrasladados): self
    {
        $this->totalImpuestosTrasladados = $totalImpuestosTrasladados;

        return $this;
    }

    public function getComplemento(): ?string
    {
        return $this->complemento;
    }

    public function setComplemento(?string $complemento): self
    {
        $this->complemento = $complemento;

        return $this;
    }

    public function getAdenda(): ?string
    {
        return $this->adenda;
    }

    public function setAdenda(?string $adenda): self
    {
        $this->adenda = $adenda;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getFolio(): ?string
    {
        return $this->folio;
    }

    public function setFolio(?string $folio): self
    {
        $this->folio = $folio;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getSello(): ?string
    {
        return $this->sello;
    }

    public function setSello(string $sello): self
    {
        $this->sello = $sello;

        return $this;
    }

    public function getFormaPago(): ?FormaPago
    {
        return $this->formaPago;
    }

    public function setFormaPago(?FormaPago $formaPago): self
    {
        $this->formaPago = $formaPago;

        return $this;
    }

    public function getNoCertificado(): ?string
    {
        return $this->noCertificado;
    }

    public function setNoCertificado(string $noCertificado): self
    {
        $this->noCertificado = $noCertificado;

        return $this;
    }

    public function getCertificado(): ?string
    {
        return $this->certificado;
    }

    public function setCertificado(string $certificado): self
    {
        $this->certificado = $certificado;

        return $this;
    }

    public function getCondicionesPago(): ?string
    {
        return $this->condicionesPago;
    }

    public function setCondicionesPago(?string $condicionesPago): self
    {
        $this->condicionesPago = $condicionesPago;

        return $this;
    }

    public function getSubTotal(): ?float
    {
        return $this->subTotal;
    }

    public function setSubTotal(float $subTotal): self
    {
        $this->subTotal = $subTotal;

        return $this;
    }

    public function getDescuento(): ?float
    {
        return $this->descuento;
    }

    public function setDescuento(?float $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getMoneda(): ?Moneda
    {
        return $this->moneda;
    }

    public function setMoneda(?Moneda $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getTipoCambio(): ?float
    {
        return $this->tipoCambio;
    }

    public function setTipoCambio(?float $tipoCambio): self
    {
        $this->tipoCambio = $tipoCambio;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getTipoComprobante(): ?TipoComprobante
    {
        return $this->tipoComprobante;
    }

    public function setTipoComprobante(?TipoComprobante $tipoComprobante): self
    {
        $this->tipoComprobante = $tipoComprobante;

        return $this;
    }

    public function getMetodoPago(): ?MetodoPago
    {
        return $this->metodoPago;
    }

    public function setMetodoPago(?MetodoPago $metodoPago): self
    {
        $this->metodoPago = $metodoPago;

        return $this;
    }

    public function getLugarExpedicion(): ?CodigoPostal
    {
        return $this->lugarExpedicion;
    }

    public function setLugarExpedicion(?CodigoPostal $lugarExpedicion): self
    {
        $this->lugarExpedicion = $lugarExpedicion;

        return $this;
    }

    public function getConfirmacion(): ?string
    {
        return $this->confirmacion;
    }

    public function setConfirmacion(?string $confirmacion): self
    {
        $this->confirmacion = $confirmacion;

        return $this;
    }

    /**
     * @return Collection|Concepto[]
     */
    public function getConceptos(): Collection
    {
        return $this->conceptos;
    }

    public function addConcepto(Concepto $concepto): self
    {
        if (!$this->conceptos->contains($concepto)) {
            $this->conceptos[] = $concepto;
            $concepto->setComprobante($this);
        }

        return $this;
    }

    public function removeConcepto(Concepto $concepto): self
    {
        if ($this->conceptos->contains($concepto)) {
            $this->conceptos->removeElement($concepto);
            // set the owning side to null (unless already changed)
            if ($concepto->getComprobante() === $this) {
                $concepto->setComprobante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CfdiRelacionados[]
     */
    public function getCfdiRelacionados(): Collection
    {
        return $this->cfdiRelacionados;
    }

    public function addCfdiRelacionado(CfdiRelacionados $cfdiRelacionado): self
    {
        if (!$this->cfdiRelacionados->contains($cfdiRelacionado)) {
            $this->cfdiRelacionados[] = $cfdiRelacionado;
            $cfdiRelacionado->setComprobante($this);
        }

        return $this;
    }

    public function removeCfdiRelacionado(CfdiRelacionados $cfdiRelacionado): self
    {
        if ($this->cfdiRelacionados->contains($cfdiRelacionado)) {
            $this->cfdiRelacionados->removeElement($cfdiRelacionado);
            // set the owning side to null (unless already changed)
            if ($cfdiRelacionado->getComprobante() === $this) {
                $cfdiRelacionado->setComprobante(null);
            }
        }

        return $this;
    }
}
