<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="admin_user")
 * @ORM\Entity(repositoryClass="App\Repository\AdminUserRepository")
 * @UniqueEntity(fields="email", message="Ese mail ya existe")
 * @UniqueEntity(fields="username", message="Usuario ya existe")
 */
class AdminUser implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30, unique=true)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="is_accountnonexpired", type="boolean")
     */
    private $isAccountNonExpired;

    /**
     * @ORM\Column(name="is_accountnonlocked", type="boolean")
     */
    private $isAccountNonLocked;

    /**
     * @ORM\Column(name="is_credentialnonexpired", type="boolean")
     */
    private $isCredentialsNonExpired;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AdminRole", inversedBy="adminUsers")
     */
    private $roles;
        
    public function __construct()
    {
        $this->isActive = true;
        $this->isAccountNonExpired = true;
        $this->isAccountNonLocked = true;
        $this->isCredentialsNonExpired = true;
        $this->roles = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->username;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()//: ?string
    {
        return $this->username;
    }

    public function setUsername(string $username)//: self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword()//: ?string
    {
        return $this->password;
    }

    public function setPassword(string $password)//: self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getEmail()//: ?string
    {
        return $this->email;
    }

    public function setEmail(string $email)//: self
    {
        $this->email = $email;

        return $this;
    }

    public function getIsActive()//: ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive)//: self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsAccountNonExpired()//: ?bool
    {
        return $this->isAccountNonExpired;
    }

    public function setIsAccountNonExpired(bool $isAccountNonExpired)//: self
    {
        $this->isAccountNonExpired = $isAccountNonExpired;

        return $this;
    }

    public function getIsAccountNonLocked()//: ?bool
    {
        return $this->isAccountNonLocked;
    }

    public function setIsAccountNonLocked(bool $isAccountNonLocked)//: self
    {
        $this->isAccountNonLocked = $isAccountNonLocked;

        return $this;
    }

    public function getIsCredentialsNonExpired()//: ?bool
    {
        return $this->isCredentialsNonExpired;
    }

    public function setIsCredentialsNonExpired(bool $isCredentialsNonExpired)//: self
    {
        $this->isCredentialsNonExpired = $isCredentialsNonExpired;

        return $this;
    }

    public function setRoles(?AdminRole $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * 
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * checks whether the user's account has expired
     */
    public function isAccountNonExpired()
    {
        return $this->isAccountNonExpired;
    }

    /**
     * checks whether the user is locked
     */
    public function isAccountNonLocked()
    {
        return $this->isAccountNonLocked;
    }

    /**
     * checks whether the user's credentials (password) has expired
     */
    public function isCredentialsNonExpired()
    {
        return $this->isCredentialsNonExpired;
    }

    /**
     * checks whether the user is enabled
     */
    public function isEnabled()
    {
        return $this->isActive;
    }

    public function eraseCredentials()
    {
    }

    /** 
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->isAccountNonExpired,
            $this->isAccountNonLocked,
            $this->isCredentialsNonExpired,
        ));
    }

    /**
     * @see \Serializable::unserialize() 
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->isAccountNonExpired,
            $this->isAccountNonLocked,
            $this->isCredentialsNonExpired,
        ) = unserialize($serialized, ['allowed_classes' => false]);
    }
}
