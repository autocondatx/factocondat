<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClaveProdServRepository")
 */
class ClaveProdServ
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $cve;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $IncluirIVATrasladado;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $IncluirIEPSTrasladado;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PalabrasSimilares;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Concepto", mappedBy="claveProdServ")
     */
    private $conceptos;

    public function __construct()
    {
        $this->conceptos = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->cve;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCve(): ?string
    {
        return $this->cve;
    }

    public function setCve(string $cve): self
    {
        $this->cve = $cve;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getIncluirIVATrasladado(): ?string
    {
        return $this->IncluirIVATrasladado;
    }

    public function setIncluirIVATrasladado(?string $IncluirIVATrasladado): self
    {
        $this->IncluirIVATrasladado = $IncluirIVATrasladado;

        return $this;
    }

    public function getIncluirIEPSTrasladado(): ?string
    {
        return $this->IncluirIEPSTrasladado;
    }

    public function setIncluirIEPSTrasladado(?string $IncluirIEPSTrasladado): self
    {
        $this->IncluirIEPSTrasladado = $IncluirIEPSTrasladado;

        return $this;
    }

    public function getPalabrasSimilares(): ?string
    {
        return $this->PalabrasSimilares;
    }

    public function setPalabrasSimilares(?string $PalabrasSimilares): self
    {
        $this->PalabrasSimilares = $PalabrasSimilares;

        return $this;
    }

    /**
     * @return Collection|Concepto[]
     */
    public function getConceptos(): Collection
    {
        return $this->conceptos;
    }

    public function addConcepto(Concepto $concepto): self
    {
        if (!$this->conceptos->contains($concepto)) {
            $this->conceptos[] = $concepto;
            $concepto->setClaveProdServ($this);
        }

        return $this;
    }

    public function removeConcepto(Concepto $concepto): self
    {
        if ($this->conceptos->contains($concepto)) {
            $this->conceptos->removeElement($concepto);
            // set the owning side to null (unless already changed)
            if ($concepto->getClaveProdServ() === $this) {
                $concepto->setClaveProdServ(null);
            }
        }

        return $this;
    }
}
