<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClaveUnidadRepository")
 */
class ClaveUnidad
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $cve;

    /**
     * @ORM\Column(type="string", length=190)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nota;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $simbolo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Concepto", mappedBy="claveUnidad")
     */
    private $conceptos;

    public function __construct()
    {
        $this->conceptos = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->cve;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCve(): ?string
    {
        return $this->cve;
    }

    public function setCve(string $cve): self
    {
        $this->cve = $cve;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getNota(): ?string
    {
        return $this->nota;
    }

    public function setNota(?string $nota): self
    {
        $this->nota = $nota;

        return $this;
    }

    public function getSimbolo(): ?string
    {
        return $this->simbolo;
    }

    public function setSimbolo(?string $simbolo): self
    {
        $this->simbolo = $simbolo;

        return $this;
    }

    /**
     * @return Collection|Concepto[]
     */
    public function getConceptos(): Collection
    {
        return $this->conceptos;
    }

    public function addConcepto(Concepto $concepto): self
    {
        if (!$this->conceptos->contains($concepto)) {
            $this->conceptos[] = $concepto;
            $concepto->setClaveUnidad($this);
        }

        return $this;
    }

    public function removeConcepto(Concepto $concepto): self
    {
        if ($this->conceptos->contains($concepto)) {
            $this->conceptos->removeElement($concepto);
            // set the owning side to null (unless already changed)
            if ($concepto->getClaveUnidad() === $this) {
                $concepto->setClaveUnidad(null);
            }
        }

        return $this;
    }
}
