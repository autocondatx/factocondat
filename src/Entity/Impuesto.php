<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImpuestoRepository")
 */
class Impuesto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $cve;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $retencion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $traslado;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TasaOCuota", mappedBy="impuesto")
     */
    private $tasas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Retencion", mappedBy="impuesto")
     */
    private $retenciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Traslado", mappedBy="impuesto")
     */
    private $traslados;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConceptoTraslados", mappedBy="impuesto")
     */
    private $conceptoTraslados;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConceptoRetenciones", mappedBy="impuesto")
     */
    private $conceptoRetenciones;

    public function __construct()
    {
        $this->tasas = new ArrayCollection();
        $this->retenciones = new ArrayCollection();
        $this->traslados = new ArrayCollection();
        $this->conceptoTraslados = new ArrayCollection();
        $this->conceptoRetenciones = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->cve;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCve(): ?string
    {
        return $this->cve;
    }

    public function setCve(string $cve): self
    {
        $this->cve = $cve;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getRetencion(): ?bool
    {
        return $this->retencion;
    }

    public function setRetencion(bool $retencion): self
    {
        $this->retencion = $retencion;

        return $this;
    }

    public function getTraslado(): ?bool
    {
        return $this->traslado;
    }

    public function setTraslado(bool $traslado): self
    {
        $this->traslado = $traslado;

        return $this;
    }

    /**
     * @return Collection|TasaOCuota[]
     */
    public function getTasas(): Collection
    {
        return $this->tasas;
    }

    public function addTasa(TasaOCuota $tasa): self
    {
        if (!$this->tasas->contains($tasa)) {
            $this->tasas[] = $tasa;
            $tasa->setImpuesto($this);
        }

        return $this;
    }

    public function removeTasa(TasaOCuota $tasa): self
    {
        if ($this->tasas->contains($tasa)) {
            $this->tasas->removeElement($tasa);
            // set the owning side to null (unless already changed)
            if ($tasa->getImpuesto() === $this) {
                $tasa->setImpuesto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Retencion[]
     */
    public function getRetenciones(): Collection
    {
        return $this->retenciones;
    }

    public function addRetencione(Retencion $retencione): self
    {
        if (!$this->retenciones->contains($retencione)) {
            $this->retenciones[] = $retencione;
            $retencione->setImpuesto($this);
        }

        return $this;
    }

    public function removeRetencione(Retencion $retencione): self
    {
        if ($this->retenciones->contains($retencione)) {
            $this->retenciones->removeElement($retencione);
            // set the owning side to null (unless already changed)
            if ($retencione->getImpuesto() === $this) {
                $retencione->setImpuesto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Traslado[]
     */
    public function getTraslados(): Collection
    {
        return $this->traslados;
    }

    public function addTraslado(Traslado $traslado): self
    {
        if (!$this->traslados->contains($traslado)) {
            $this->traslados[] = $traslado;
            $traslado->setImpuesto($this);
        }

        return $this;
    }

    public function removeTraslado(Traslado $traslado): self
    {
        if ($this->traslados->contains($traslado)) {
            $this->traslados->removeElement($traslado);
            // set the owning side to null (unless already changed)
            if ($traslado->getImpuesto() === $this) {
                $traslado->setImpuesto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ConceptoTraslados[]
     */
    public function getConceptoTraslados(): Collection
    {
        return $this->conceptoTraslados;
    }

    public function addConceptoTraslado(ConceptoTraslados $conceptoTraslado): self
    {
        if (!$this->conceptoTraslados->contains($conceptoTraslado)) {
            $this->conceptoTraslados[] = $conceptoTraslado;
            $conceptoTraslado->setImpuesto($this);
        }

        return $this;
    }

    public function removeConceptoTraslado(ConceptoTraslados $conceptoTraslado): self
    {
        if ($this->conceptoTraslados->contains($conceptoTraslado)) {
            $this->conceptoTraslados->removeElement($conceptoTraslado);
            // set the owning side to null (unless already changed)
            if ($conceptoTraslado->getImpuesto() === $this) {
                $conceptoTraslado->setImpuesto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ConceptoRetenciones[]
     */
    public function getConceptoRetenciones(): Collection
    {
        return $this->conceptoRetenciones;
    }

    public function addConceptoRetencione(ConceptoRetenciones $conceptoRetencione): self
    {
        if (!$this->conceptoRetenciones->contains($conceptoRetencione)) {
            $this->conceptoRetenciones[] = $conceptoRetencione;
            $conceptoRetencione->setImpuesto($this);
        }

        return $this;
    }

    public function removeConceptoRetencione(ConceptoRetenciones $conceptoRetencione): self
    {
        if ($this->conceptoRetenciones->contains($conceptoRetencione)) {
            $this->conceptoRetenciones->removeElement($conceptoRetencione);
            // set the owning side to null (unless already changed)
            if ($conceptoRetencione->getImpuesto() === $this) {
                $conceptoRetencione->setImpuesto(null);
            }
        }

        return $this;
    }
}
