<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConceptoTrasladosRepository")
 */
class ConceptoTraslados
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $base;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Impuesto", inversedBy="conceptoTraslados")
     * @ORM\JoinColumn(nullable=false)
     */
    private $impuesto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoFactor", inversedBy="conceptoTraslados")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipoFactor;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tasaOCuota;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $importe;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Concepto", inversedBy="traslados")
     * @ORM\JoinColumn(nullable=false)
     */
    private $concepto;

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBase(): ?float
    {
        return $this->base;
    }

    public function setBase(float $base): self
    {
        $this->base = $base;

        return $this;
    }

    public function getImpuesto(): ?Impuesto
    {
        return $this->impuesto;
    }

    public function setImpuesto(?Impuesto $impuesto): self
    {
        $this->impuesto = $impuesto;

        return $this;
    }

    public function getTipoFactor(): ?TipoFactor
    {
        return $this->tipoFactor;
    }

    public function setTipoFactor(?TipoFactor $tipoFactor): self
    {
        $this->tipoFactor = $tipoFactor;

        return $this;
    }

    public function getTasaOCuota(): ?float
    {
        return $this->tasaOCuota;
    }

    public function setTasaOCuota(?float $tasaOCuota): self
    {
        $this->tasaOCuota = $tasaOCuota;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(?float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getConcepto(): ?Concepto
    {
        return $this->concepto;
    }

    public function setConcepto(?Concepto $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }
}
