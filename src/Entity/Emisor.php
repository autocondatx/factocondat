<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmisorRepository")
 */
class Emisor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $RFC;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RegimenFiscal", inversedBy="emisores")
     * @ORM\JoinColumn(nullable=false)
     */
    private $regimenFiscal;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Receptor", mappedBy="emisor", orphanRemoval=true)
     */
    private $receptores;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comprobante", mappedBy="emisor", orphanRemoval=true)
     */
    private $comprobantes;

    public function __construct()
    {
        $this->receptores = new ArrayCollection();
        $this->comprobantes = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRFC(): ?string
    {
        return $this->RFC;
    }

    public function setRFC(string $RFC): self
    {
        $this->RFC = $RFC;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRegimenFiscal(): ?RegimenFiscal
    {
        return $this->regimenFiscal;
    }

    public function setRegimenFiscal(?RegimenFiscal $regimenFiscal): self
    {
        $this->regimenFiscal = $regimenFiscal;

        return $this;
    }

    /**
     * @return Collection|Receptor[]
     */
    public function getReceptores(): Collection
    {
        return $this->receptores;
    }

    public function addReceptore(Receptor $receptore): self
    {
        if (!$this->receptores->contains($receptore)) {
            $this->receptores[] = $receptore;
            $receptore->setEmisor($this);
        }

        return $this;
    }

    public function removeReceptore(Receptor $receptore): self
    {
        if ($this->receptores->contains($receptore)) {
            $this->receptores->removeElement($receptore);
            // set the owning side to null (unless already changed)
            if ($receptore->getEmisor() === $this) {
                $receptore->setEmisor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comprobante[]
     */
    public function getComprobantes(): Collection
    {
        return $this->comprobantes;
    }

    public function addComprobante(Comprobante $comprobante): self
    {
        if (!$this->comprobantes->contains($comprobante)) {
            $this->comprobantes[] = $comprobante;
            $comprobante->setEmisor($this);
        }

        return $this;
    }

    public function removeComprobante(Comprobante $comprobante): self
    {
        if ($this->comprobantes->contains($comprobante)) {
            $this->comprobantes->removeElement($comprobante);
            // set the owning side to null (unless already changed)
            if ($comprobante->getEmisor() === $this) {
                $comprobante->setEmisor(null);
            }
        }

        return $this;
    }
}
