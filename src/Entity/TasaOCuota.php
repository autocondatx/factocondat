<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TasaOCuotaRepository")
 */
class TasaOCuota
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $rangoFijo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $valMin;

    /**
     * @ORM\Column(type="float")
     */
    private $vMax;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Impuesto", inversedBy="tasas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $impuesto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoFactor", inversedBy="tasas")
     */
    private $tipoFator;

    /**
     * @ORM\Column(type="boolean")
     */
    private $traslado;

    /**
     * @ORM\Column(type="boolean")
     */
    private $retencion;

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRangoFijo(): ?string
    {
        return $this->rangoFijo;
    }

    public function setRangoFijo(string $rangoFijo): self
    {
        $this->rangoFijo = $rangoFijo;

        return $this;
    }

    public function getValMin(): ?float
    {
        return $this->valMin;
    }

    public function setValMin(?float $valMin): self
    {
        $this->valMin = $valMin;

        return $this;
    }

    public function getVMax(): ?float
    {
        return $this->vMax;
    }

    public function setVMax(float $vMax): self
    {
        $this->vMax = $vMax;

        return $this;
    }

    public function getImpuesto(): ?Impuesto
    {
        return $this->impuesto;
    }

    public function setImpuesto(?Impuesto $impuesto): self
    {
        $this->impuesto = $impuesto;

        return $this;
    }

    public function getTipoFator(): ?TipoFactor
    {
        return $this->tipoFator;
    }

    public function setTipoFator(?TipoFactor $tipoFator): self
    {
        $this->tipoFator = $tipoFator;

        return $this;
    }

    public function getTraslado(): ?bool
    {
        return $this->traslado;
    }

    public function setTraslado(bool $traslado): self
    {
        $this->traslado = $traslado;

        return $this;
    }

    public function getRetencion(): ?bool
    {
        return $this->retencion;
    }

    public function setRetencion(bool $retencion): self
    {
        $this->retencion = $retencion;

        return $this;
    }
}
