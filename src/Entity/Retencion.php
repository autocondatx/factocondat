<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RetencionRepository")
 */
class Retencion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Impuesto", inversedBy="retenciones")
     * @ORM\JoinColumn(nullable=false)
     */
    private $impuesto;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Comprobante", inversedBy="retenciones")
     * @ORM\JoinColumn(nullable=false)
     */
    private $comprobante;

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getImpuesto(): ?Impuesto
    {
        return $this->impuesto;
    }

    public function setImpuesto(?Impuesto $impuesto): self
    {
        $this->impuesto = $impuesto;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getComprobante(): ?Comprobante
    {
        return $this->comprobante;
    }

    public function setComprobante(?Comprobante $comprobante): self
    {
        $this->comprobante = $comprobante;

        return $this;
    }
}
