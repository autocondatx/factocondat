<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdminRoleRepository")
 */
class AdminRole
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AdminUser", mappedBy="roles")
     */
    private $adminUsers;

    public function __construct()
    {
        $this->adminUsers = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->role;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = "ROLE_" . strtoupper($role);

        return $this;
    }

    /**
     * @return Collection|AdminUser[]
     */
    public function getAdminUsers(): Collection
    {
        return $this->adminUsers;
    }

    public function addAdminUser(AdminUser $adminUser): self
    {
        if (!$this->adminUsers->contains($adminUser)) {
            $this->adminUsers[] = $adminUser;
            $adminUser->setRoles($this);
        }

        return $this;
    }

    public function removeAdminUser(AdminUser $adminUser): self
    {
        if ($this->adminUsers->contains($adminUser)) {
            $this->adminUsers->removeElement($adminUser);
            // set the owning side to null (unless already changed)
            if ($adminUser->getRoles() === $this) {
                $adminUser->setRoles(null);
            }
        }

        return $this;
    }

}
