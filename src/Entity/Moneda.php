<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MonedaRepository")
 */
class Moneda
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $cve;

    /**
     * @ORM\Column(type="string", length=160)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $decimales;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $perVariacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comprobante", mappedBy="moneda")
     */
    private $comprobantes;

    public function __construct()
    {
        $this->comprobantes = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->cve;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCve(): ?string
    {
        return $this->cve;
    }

    public function setCve(string $cve): self
    {
        $this->cve = $cve;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getDecimales(): ?int
    {
        return $this->decimales;
    }

    public function setDecimales(int $decimales): self
    {
        $this->decimales = $decimales;

        return $this;
    }

    public function getPerVariacion(): ?int
    {
        return $this->perVariacion;
    }

    public function setPerVariacion(?int $perVariacion): self
    {
        $this->perVariacion = $perVariacion;

        return $this;
    }

    /**
     * @return Collection|Comprobante[]
     */
    public function getComprobantes(): Collection
    {
        return $this->comprobantes;
    }

    public function addComprobante(Comprobante $comprobante): self
    {
        if (!$this->comprobantes->contains($comprobante)) {
            $this->comprobantes[] = $comprobante;
            $comprobante->setMoneda($this);
        }

        return $this;
    }

    public function removeComprobante(Comprobante $comprobante): self
    {
        if ($this->comprobantes->contains($comprobante)) {
            $this->comprobantes->removeElement($comprobante);
            // set the owning side to null (unless already changed)
            if ($comprobante->getMoneda() === $this) {
                $comprobante->setMoneda(null);
            }
        }

        return $this;
    }
}
