<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrasladoRepository")
 */
class Traslado
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Impuesto", inversedBy="traslados")
     */
    private $impuesto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoFactor", inversedBy="traslados")
     */
    private $tipoFactor;

    /**
     * @ORM\Column(type="float")
     */
    private $tasaOCuota;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Comprobante", inversedBy="traslados")
     * @ORM\JoinColumn(nullable=false)
     */
    private $comprobante;

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getImpuesto(): ?Impuesto
    {
        return $this->impuesto;
    }

    public function setImpuesto(?Impuesto $impuesto): self
    {
        $this->impuesto = $impuesto;

        return $this;
    }

    public function getTipoFactor(): ?TipoFactor
    {
        return $this->tipoFactor;
    }

    public function setTipoFactor(?TipoFactor $tipoFactor): self
    {
        $this->tipoFactor = $tipoFactor;

        return $this;
    }

    public function getTasaOCuota(): ?float
    {
        return $this->tasaOCuota;
    }

    public function setTasaOCuota(float $tasaOCuota): self
    {
        $this->tasaOCuota = $tasaOCuota;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getComprobante(): ?Comprobante
    {
        return $this->comprobante;
    }

    public function setComprobante(?Comprobante $comprobante): self
    {
        $this->comprobante = $comprobante;

        return $this;
    }
}
