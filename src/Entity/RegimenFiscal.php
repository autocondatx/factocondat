<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RegimenFiscalRepository")
 */
class RegimenFiscal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $cve;

    /**
     * @ORM\Column(type="string", length=160)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pFisica;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pMoral;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Emisor", mappedBy="regimenFiscal", orphanRemoval=true)
     */
    private $emisores;

    public function __construct()
    {
        $this->emisores = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->cve;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCve(): ?string
    {
        return $this->cve;
    }

    public function setCve(string $cve): self
    {
        $this->cve = $cve;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPFisica(): ?bool
    {
        return $this->pFisica;
    }

    public function setPFisica(bool $pFisica): self
    {
        $this->pFisica = $pFisica;

        return $this;
    }

    public function getPMoral(): ?bool
    {
        return $this->pMoral;
    }

    public function setPMoral(bool $pMoral): self
    {
        $this->pMoral = $pMoral;

        return $this;
    }

    /**
     * @return Collection|Emisor[]
     */
    public function getEmisores(): Collection
    {
        return $this->emisores;
    }

    public function addEmisore(Emisor $emisore): self
    {
        if (!$this->emisores->contains($emisore)) {
            $this->emisores[] = $emisore;
            $emisore->setRegimenFiscal($this);
        }

        return $this;
    }

    public function removeEmisore(Emisor $emisore): self
    {
        if ($this->emisores->contains($emisore)) {
            $this->emisores->removeElement($emisore);
            // set the owning side to null (unless already changed)
            if ($emisore->getRegimenFiscal() === $this) {
                $emisore->setRegimenFiscal(null);
            }
        }

        return $this;
    }
}
