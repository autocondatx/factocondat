<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TipoFactorRepository")
 */
class TipoFactor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=90)
     */
    private $tipo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TasaOCuota", mappedBy="tipoFator")
     */
    private $tasas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Traslado", mappedBy="tipoFactor")
     */
    private $traslados;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConceptoTraslados", mappedBy="tipoFactor")
     */
    private $conceptoTraslados;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConceptoRetenciones", mappedBy="tipoFactor")
     */
    private $conceptoRetenciones;

    public function __construct()
    {
        $this->tasas = new ArrayCollection();
        $this->traslados = new ArrayCollection();
        $this->conceptoTraslados = new ArrayCollection();
        $this->conceptoRetenciones = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->tipo;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * @return Collection|TasaOCuota[]
     */
    public function getTasas(): Collection
    {
        return $this->tasas;
    }

    public function addTasa(TasaOCuota $tasa): self
    {
        if (!$this->tasas->contains($tasa)) {
            $this->tasas[] = $tasa;
            $tasa->setTipoFator($this);
        }

        return $this;
    }

    public function removeTasa(TasaOCuota $tasa): self
    {
        if ($this->tasas->contains($tasa)) {
            $this->tasas->removeElement($tasa);
            // set the owning side to null (unless already changed)
            if ($tasa->getTipoFator() === $this) {
                $tasa->setTipoFator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Traslado[]
     */
    public function getTraslados(): Collection
    {
        return $this->traslados;
    }

    public function addTraslado(Traslado $traslado): self
    {
        if (!$this->traslados->contains($traslado)) {
            $this->traslados[] = $traslado;
            $traslado->setTipoFactor($this);
        }

        return $this;
    }

    public function removeTraslado(Traslado $traslado): self
    {
        if ($this->traslados->contains($traslado)) {
            $this->traslados->removeElement($traslado);
            // set the owning side to null (unless already changed)
            if ($traslado->getTipoFactor() === $this) {
                $traslado->setTipoFactor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ConceptoTraslados[]
     */
    public function getConceptoTraslados(): Collection
    {
        return $this->conceptoTraslados;
    }

    public function addConceptoTraslado(ConceptoTraslados $conceptoTraslado): self
    {
        if (!$this->conceptoTraslados->contains($conceptoTraslado)) {
            $this->conceptoTraslados[] = $conceptoTraslado;
            $conceptoTraslado->setTipoFactor($this);
        }

        return $this;
    }

    public function removeConceptoTraslado(ConceptoTraslados $conceptoTraslado): self
    {
        if ($this->conceptoTraslados->contains($conceptoTraslado)) {
            $this->conceptoTraslados->removeElement($conceptoTraslado);
            // set the owning side to null (unless already changed)
            if ($conceptoTraslado->getTipoFactor() === $this) {
                $conceptoTraslado->setTipoFactor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ConceptoRetenciones[]
     */
    public function getConceptoRetenciones(): Collection
    {
        return $this->conceptoRetenciones;
    }

    public function addConceptoRetencione(ConceptoRetenciones $conceptoRetencione): self
    {
        if (!$this->conceptoRetenciones->contains($conceptoRetencione)) {
            $this->conceptoRetenciones[] = $conceptoRetencione;
            $conceptoRetencione->setTipoFactor($this);
        }

        return $this;
    }

    public function removeConceptoRetencione(ConceptoRetenciones $conceptoRetencione): self
    {
        if ($this->conceptoRetenciones->contains($conceptoRetencione)) {
            $this->conceptoRetenciones->removeElement($conceptoRetencione);
            // set the owning side to null (unless already changed)
            if ($conceptoRetencione->getTipoFactor() === $this) {
                $conceptoRetencione->setTipoFactor(null);
            }
        }

        return $this;
    }
}
