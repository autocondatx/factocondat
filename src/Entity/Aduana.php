<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AduanaRepository")
 */
class Aduana
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $cve;

    /**
     * @ORM\Column(type="string", length=190)
     */
    private $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NumPedimientoAduana", mappedBy="aduana", orphanRemoval=true)
     */
    private $numsPedimiento;

    public function __construct()
    {
        $this->numsPedimiento = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->cve;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCve(): ?string
    {
        return $this->cve;
    }

    public function setCve(string $cve): self
    {
        $this->cve = $cve;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return Collection|NumPedimientoAduana[]
     */
    public function getNumsPedimiento(): Collection
    {
        return $this->numsPedimiento;
    }

    public function addNumsPedimiento(NumPedimientoAduana $numsPedimiento): self
    {
        if (!$this->numsPedimiento->contains($numsPedimiento)) {
            $this->numsPedimiento[] = $numsPedimiento;
            $numsPedimiento->setAduana($this);
        }

        return $this;
    }

    public function removeNumsPedimiento(NumPedimientoAduana $numsPedimiento): self
    {
        if ($this->numsPedimiento->contains($numsPedimiento)) {
            $this->numsPedimiento->removeElement($numsPedimiento);
            // set the owning side to null (unless already changed)
            if ($numsPedimiento->getAduana() === $this) {
                $numsPedimiento->setAduana(null);
            }
        }

        return $this;
    }
}
