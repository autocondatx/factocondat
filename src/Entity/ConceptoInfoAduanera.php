<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConceptoInfoAduaneraRepository")
 */
class ConceptoInfoAduanera
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=21)
     */
    private $numeroPedimento;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Concepto", inversedBy="infoAduanera")
     * @ORM\JoinColumn(nullable=false)
     */
    private $concepto;

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNumeroPedimento(): ?string
    {
        return $this->numeroPedimento;
    }

    public function setNumeroPedimento(string $numeroPedimento): self
    {
        $this->numeroPedimento = $numeroPedimento;

        return $this;
    }

    public function getConcepto(): ?Concepto
    {
        return $this->concepto;
    }

    public function setConcepto(?Concepto $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }
}
