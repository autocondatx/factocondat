<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NumPedimientoAduanaRepository")
 */
class NumPedimientoAduana
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Aduana", inversedBy="numsPedimiento")
     * @ORM\JoinColumn(nullable=false)
     */
    private $aduana;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $ejercicio;

    /**
     * @ORM\Column(type="integer")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PatenteAduanal", inversedBy="numsPedimiento")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patente;

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAduana(): ?Aduana
    {
        return $this->aduana;
    }

    public function setAduana(?Aduana $aduana): self
    {
        $this->aduana = $aduana;

        return $this;
    }

    public function getEjercicio(): ?string
    {
        return $this->ejercicio;
    }

    public function setEjercicio(string $ejercicio): self
    {
        $this->ejercicio = $ejercicio;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getPatente(): ?PatenteAduanal
    {
        return $this->patente;
    }

    public function setPatente(?PatenteAduanal $patente): self
    {
        $this->patente = $patente;

        return $this;
    }
}
