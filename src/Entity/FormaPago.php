<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormaPagoRepository")
 */
class FormaPago
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $cve;

    /**
     * @ORM\Column(type="string", length=190)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $bancarizado;

    /**
     * @ORM\Column(type="boolean")
     */
    private $RFCEmisorCtaOrdenante;

    /**
     * @ORM\Column(type="boolean")
     */
    private $CuentaOrdenante;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $PatronCtaOrdenante;

    /**
     * @ORM\Column(type="boolean")
     */
    private $RFCEmisorCtaBeneficiario;

    /**
     * @ORM\Column(type="boolean")
     */
    private $CuentaBeneficiario;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $PatronCtaBeneficiario;

    /**
     * @ORM\Column(type="boolean")
     */
    private $TipoCadenaPago;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $BancoEmisorExtranjero;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comprobante", mappedBy="formaPago")
     */
    private $comprobantes;

    public function __construct()
    {
        $this->comprobantes = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->cve;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCve(): ?string
    {
        return $this->cve;
    }

    public function setCve(string $cve): self
    {
        $this->cve = $cve;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getBancarizado(): ?bool
    {
        return $this->bancarizado;
    }

    public function setBancarizado(bool $bancarizado): self
    {
        $this->bancarizado = $bancarizado;

        return $this;
    }

    public function getRFCEmisorCtaOrdenante(): ?bool
    {
        return $this->RFCEmisorCtaOrdenante;
    }

    public function setRFCEmisorCtaOrdenante(bool $RFCEmisorCtaOrdenante): self
    {
        $this->RFCEmisorCtaOrdenante = $RFCEmisorCtaOrdenante;

        return $this;
    }

    public function getCuentaOrdenante(): ?bool
    {
        return $this->CuentaOrdenante;
    }

    public function setCuentaOrdenante(bool $CuentaOrdenante): self
    {
        $this->CuentaOrdenante = $CuentaOrdenante;

        return $this;
    }

    public function getPatronCtaOrdenante(): ?string
    {
        return $this->PatronCtaOrdenante;
    }

    public function setPatronCtaOrdenante(?string $PatronCtaOrdenante): self
    {
        $this->PatronCtaOrdenante = $PatronCtaOrdenante;

        return $this;
    }

    public function getRFCEmisorCtaBeneficiario(): ?bool
    {
        return $this->RFCEmisorCtaBeneficiario;
    }

    public function setRFCEmisorCtaBeneficiario(bool $RFCEmisorCtaBeneficiario): self
    {
        $this->RFCEmisorCtaBeneficiario = $RFCEmisorCtaBeneficiario;

        return $this;
    }

    public function getCuentaBeneficiario(): ?bool
    {
        return $this->CuentaBeneficiario;
    }

    public function setCuentaBeneficiario(bool $CuentaBeneficiario): self
    {
        $this->CuentaBeneficiario = $CuentaBeneficiario;

        return $this;
    }

    public function getPatronCtaBeneficiario(): ?string
    {
        return $this->PatronCtaBeneficiario;
    }

    public function setPatronCtaBeneficiario(?string $PatronCtaBeneficiario): self
    {
        $this->PatronCtaBeneficiario = $PatronCtaBeneficiario;

        return $this;
    }

    public function getTipoCadenaPago(): ?bool
    {
        return $this->TipoCadenaPago;
    }

    public function setTipoCadenaPago(bool $TipoCadenaPago): self
    {
        $this->TipoCadenaPago = $TipoCadenaPago;

        return $this;
    }

    public function getBancoEmisorExtranjero(): ?string
    {
        return $this->BancoEmisorExtranjero;
    }

    public function setBancoEmisorExtranjero(?string $BancoEmisorExtranjero): self
    {
        $this->BancoEmisorExtranjero = $BancoEmisorExtranjero;

        return $this;
    }

    /**
     * @return Collection|Comprobante[]
     */
    public function getComprobantes(): Collection
    {
        return $this->comprobantes;
    }

    public function addComprobante(Comprobante $comprobante): self
    {
        if (!$this->comprobantes->contains($comprobante)) {
            $this->comprobantes[] = $comprobante;
            $comprobante->setFormaPago($this);
        }

        return $this;
    }

    public function removeComprobante(Comprobante $comprobante): self
    {
        if ($this->comprobantes->contains($comprobante)) {
            $this->comprobantes->removeElement($comprobante);
            // set the owning side to null (unless already changed)
            if ($comprobante->getFormaPago() === $this) {
                $comprobante->setFormaPago(null);
            }
        }

        return $this;
    }
}
