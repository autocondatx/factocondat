<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReceptorRepository")
 */
class Receptor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Emisor", inversedBy="receptores")
     * @ORM\JoinColumn(nullable=false)
     */
    private $emisor;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $RFC;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pais", inversedBy="receptores")
     */
    private $recidenciaFiscal;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $numRegIdTrib;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UsoCFDI", inversedBy="receptores")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usoCFDI;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comprobante", mappedBy="receptor", orphanRemoval=true)
     */
    private $comprobantes;

    public function __construct()
    {
        $this->comprobantes = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmisor(): ?Emisor
    {
        return $this->emisor;
    }

    public function setEmisor(?Emisor $emisor): self
    {
        $this->emisor = $emisor;

        return $this;
    }

    public function getRFC(): ?string
    {
        return $this->RFC;
    }

    public function setRFC(string $RFC): self
    {
        $this->RFC = $RFC;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRecidenciaFiscal(): ?Pais
    {
        return $this->recidenciaFiscal;
    }

    public function setRecidenciaFiscal(?Pais $recidenciaFiscal): self
    {
        $this->recidenciaFiscal = $recidenciaFiscal;

        return $this;
    }

    public function getNumRegIdTrib(): ?string
    {
        return $this->numRegIdTrib;
    }

    public function setNumRegIdTrib(?string $numRegIdTrib): self
    {
        $this->numRegIdTrib = $numRegIdTrib;

        return $this;
    }

    public function getUsoCFDI(): ?UsoCFDI
    {
        return $this->usoCFDI;
    }

    public function setUsoCFDI(?UsoCFDI $usoCFDI): self
    {
        $this->usoCFDI = $usoCFDI;

        return $this;
    }

    /**
     * @return Collection|Comprobante[]
     */
    public function getComprobantes(): Collection
    {
        return $this->comprobantes;
    }

    public function addComprobante(Comprobante $comprobante): self
    {
        if (!$this->comprobantes->contains($comprobante)) {
            $this->comprobantes[] = $comprobante;
            $comprobante->setReceptor($this);
        }

        return $this;
    }

    public function removeComprobante(Comprobante $comprobante): self
    {
        if ($this->comprobantes->contains($comprobante)) {
            $this->comprobantes->removeElement($comprobante);
            // set the owning side to null (unless already changed)
            if ($comprobante->getReceptor() === $this) {
                $comprobante->setReceptor(null);
            }
        }

        return $this;
    }
}
