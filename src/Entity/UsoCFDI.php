<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsoCFDIRepository")
 */
class UsoCFDI
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $usoCFDI;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pFisica;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pMoral;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Receptor", mappedBy="usoCFDI")
     */
    private $receptores;

    public function __construct()
    {
        $this->receptores = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->usoCFDI;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsoCFDI(): ?string
    {
        return $this->usoCFDI;
    }

    public function setUsoCFDI(string $usoCFDI): self
    {
        $this->usoCFDI = $usoCFDI;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPFisica(): ?bool
    {
        return $this->pFisica;
    }

    public function setPFisica(bool $pFisica): self
    {
        $this->pFisica = $pFisica;

        return $this;
    }

    public function getPMoral(): ?bool
    {
        return $this->pMoral;
    }

    public function setPMoral(bool $pMoral): self
    {
        $this->pMoral = $pMoral;

        return $this;
    }

    /**
     * @return Collection|Receptor[]
     */
    public function getReceptores(): Collection
    {
        return $this->receptores;
    }

    public function addReceptore(Receptor $receptore): self
    {
        if (!$this->receptores->contains($receptore)) {
            $this->receptores[] = $receptore;
            $receptore->setUsoCFDI($this);
        }

        return $this;
    }

    public function removeReceptore(Receptor $receptore): self
    {
        if ($this->receptores->contains($receptore)) {
            $this->receptores->removeElement($receptore);
            // set the owning side to null (unless already changed)
            if ($receptore->getUsoCFDI() === $this) {
                $receptore->setUsoCFDI(null);
            }
        }

        return $this;
    }
}
