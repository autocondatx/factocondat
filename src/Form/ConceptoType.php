<?php

namespace App\Form;

use App\Entity\Concepto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConceptoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cuentaPredialNum')
            ->add('complementos')
            ->add('partes')
            ->add('noIdentificacion')
            ->add('cantidad')
            ->add('unidad')
            ->add('descripcion')
            ->add('valorUnitario')
            ->add('importe')
            ->add('descuento')
            ->add('claveProdServ')
            ->add('claveUnidad')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Concepto::class,
        ]);
    }
}
