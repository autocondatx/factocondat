<?php

namespace App\Form;

use App\Entity\ConceptoRetenciones;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConceptoRetencionesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('base')
            ->add('tasaOCuota')
            ->add('importe')
            ->add('impuesto')
            ->add('tipoFactor')
            ->add('concepto')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ConceptoRetenciones::class,
        ]);
    }
}
