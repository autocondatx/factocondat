<?php

namespace App\Form;

use App\Entity\Comprobante;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ComprobanteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('emisor')
            ->add('receptor')
            ->add('cfdiRelacionados')
            ->add('conceptos', CollectionType::class, array(
                'entry_type' => ConceptoType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('totalImpuestosRetenidos')
            ->add('totalImpuestosTrasladados')
            ->add('complemento')
            ->add('adenda')
            ->add('version')
            ->add('serie')
            ->add('folio')
            ->add('fecha')
            ->add('sello')
            ->add('noCertificado')
            ->add('certificado')
            ->add('condicionesPago')
            ->add('subTotal')
            ->add('descuento')
            ->add('tipoCambio')
            ->add('total')
            ->add('confirmacion')
            ->add('formaPago')
            ->add('moneda')
            ->add('tipoComprobante')
            ->add('metodoPago')
            ->add('lugarExpedicion')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comprobante::class,
        ]);
    }
}
