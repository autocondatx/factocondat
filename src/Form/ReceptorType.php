<?php

namespace App\Form;

use App\Entity\Receptor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReceptorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('RFC')
            ->add('nombre')
            ->add('numRegIdTrib')
            ->add('emisor')
            ->add('recidenciaFiscal')
            ->add('usoCFDI')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Receptor::class,
        ]);
    }
}
