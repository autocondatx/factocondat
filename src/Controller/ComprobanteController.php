<?php

namespace App\Controller;

use App\Entity\Comprobante;
use App\Form\ComprobanteType;
use App\Repository\ComprobanteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comprobante")
 */
class ComprobanteController extends Controller
{
    /**
     * @Route("/", name="comprobante_index", methods="GET")
     */
    public function index(ComprobanteRepository $comprobanteRepository): Response
    {
        return $this->render('comprobante/index.html.twig', ['comprobantes' => $comprobanteRepository->findAll()]);
    }

    /**
     * @Route("/new", name="comprobante_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $comprobante = new Comprobante();
        $form = $this->createForm(ComprobanteType::class, $comprobante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comprobante);
            $em->flush();

            return $this->redirectToRoute('comprobante_index');
        }

        return $this->render('comprobante/new.html.twig', [
            'comprobante' => $comprobante,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comprobante_show", methods="GET")
     */
    public function show(Comprobante $comprobante): Response
    {
        return $this->render('comprobante/show.html.twig', ['comprobante' => $comprobante]);
    }

    /**
     * @Route("/{id}/edit", name="comprobante_edit", methods="GET|POST")
     */
    public function edit(Request $request, Comprobante $comprobante): Response
    {
        $form = $this->createForm(ComprobanteType::class, $comprobante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comprobante_edit', ['id' => $comprobante->getId()]);
        }

        return $this->render('comprobante/edit.html.twig', [
            'comprobante' => $comprobante,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comprobante_delete", methods="DELETE")
     */
    public function delete(Request $request, Comprobante $comprobante): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comprobante->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comprobante);
            $em->flush();
        }

        return $this->redirectToRoute('comprobante_index');
    }
}
