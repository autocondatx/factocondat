<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
     /**
      * @Route("/", name="index")
      */
    public function indexAction(Request $request)
    {
        return $this->render(
            'web/index.html.twig',
            array()
        );
    }

    /**
      * @Route("/dashboard", name="dashboard")
      */
    public function dashboard()
    {
        return $this->render(
            'dashboard/main.html.twig',
            array()
        );
    }}
