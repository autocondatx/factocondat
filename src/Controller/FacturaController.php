<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\HttpKernel\Kernel;
use App\Controller\FacturacionModerna;

/**
 * Description of FacturaController
 *
 * @author erparom
 * @Route("/admin/factura")
 */
class FacturaController extends Controller
{
    /**
     * Niveles de debug:
     * 0 - No almacenar
     * 1 - Almacenar mensajes SOAP en archivo log
     * 
     * @var integer 
     */
    private $debugLevel;
    
    /**
     * 
     * @var string
     */
    private $rfcEmisor;
    
    /**
     *
     * @var string
     */
    private $timbradoURL;
    
    /**
     * @var string
     */
    private $userId;
    
    /**
     *
     * @var string
     */
    private $password;

    /**
     *
     * @var string
     */
    private $cfdi;

    public function __construct(
            $debugLevel = 1, 
            $rfcEmisor = "TCM970625MB1", 
            $timbradoURL = "https://t1demo.facturacionmoderna.com/timbrado/wsdl",
            $userId = "UsuarioPruebasWS",
            $password = "b9ec2afa3361a59af4b4d102d3f704eabdf097d4"
    
        )
    {
        $this->debugLevel = $debugLevel;
        $this->rfcEmisor = $rfcEmisor;
        $this->timbradoURL = $timbradoURL;
        $this->userId = $userId;
        $this->password = $password;
    }
            
    
    /**
      * @Route("/", name="factura_index")
      */
    public function indexAction(Request $request)
    {
        $xmlFile = "/home/erparom/public_html/factocondat/docs/cfdv33base.xml";
        $xslFile = "/home/erparom/public_html/factocondat/docs/cadenaoriginal_3_3.xslt";
        
        //$xml = $this->generarXML($array);
        //$cadena = $this->generarCadena($xslFile, $xmlFile);
        
        $timbado = $this->timbrar();

        return $this->render('comprobante/test.html.twig', array(
            //'cadena' => $cadena,
            'timbado' => $timbado,
        ));
    }

    private function encodeBase64($string)
    {
        return \base64_encode($string);
    }

    private function generarXML($array)
    {
$xmlstr = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<cfdi:Comprobante
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:cfdi="http://www.sat.gob.mx/cfd/3"
    xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd"
    Version="3.3"
    Serie="A"
    Folio="{$array['folio']}"
    Fecha="{$array['fecha']}" 
    Moneda="MXN"
    SubTotal="{$array['subtotal']}"
    Total="{$array['total']}"
    Descuento="{$array['descuento']}"
    TipoCambio="1.0"
    TipoDeComprobante="I"
    FormaPago="{$array['formaPago']}"
    MetodoPago="PUE"
    CondicionesDePago="{$array['condiciones']}"
    LugarExpedicion="{$array['lugarExpedicion']}"
    Confirmacion="{$array['confirmacion']}"
    NoCertificado="{$array['noCertificado']}"
    Sello="{$array['sello']}"
    Certificado="{$array['certficado']}" >
    <cfdi:CfdiRelacionados TipoRelacion="02">
    </cfdi:CfdiRelacionados>
    <cfdi:Emisor Rfc="{$array['emisorRfc']}" Nombre="{$array['emisorNombre']}" RegimenFiscal="{$array['emisorRegimen']}"/>
    <cfdi:Receptor Rfc="{$array['receptorRfc']}" Nombre="{$array['receptorNombre']}" ResidenciaFiscal="{$array['receptorResidencia']}" NumRegIdTrib="{$array['receptorNumRegIdTrib']}" UsoCFDI="{$array['receptorUsoCFDI']}"/>
    <cfdi:Conceptos>
        <cfdi:Concepto ClaveProdServ="{$array['conceptoClaveProdServ']}" ClaveUnidad="{$array['conceptoClaveUnidad']}" NoIdentificacion="{$array['conceptoNoIdentificacion']}" Cantidad="{$array['conceptoCantidad']}" Unidad="{$array['conceptoUnidad']}" Descripcion="{$array['conceptoDescripcion']}" ValorUnitario="{$array['conceptoValorUnitario']}" Importe="{$array['conceptoImporte']}">
            <cfdi:Impuestos>
                <cfdi:Traslados>
                    <cfdi:Traslado Base="2250000" Impuesto="002" TipoFactor="Tasa" TasaOCuota="1.600000" Importe="360000"/>
                </cfdi:Traslados>
                <cfdi:Retenciones>
                    <cfdi:Retencion Base="2250000" Impuesto="001" TipoFactor="Tasa" TasaOCuota="0.300000" Importe="247500"/>
                </cfdi:Retenciones>
            </cfdi:Impuestos>
            <cfdi:CuentaPredial Numero="{$array['conceptoNumeroCuentaPredial']}"/>
        </cfdi:Concepto>
    </cfdi:Conceptos>
    <cfdi:Impuestos TotalImpuestosRetenidos="{$array['totalImpuestosRetenidos']}" TotalImpuestosTrasladados="{$array['totalImpuestosTrasladados']}">
        <cfdi:Retenciones>
            <cfdi:Retencion Impuesto="001" Importe="247000"/>
        </cfdi:Retenciones>
        <cfdi:Traslados>
            <cfdi:Traslado Impuesto="002" TipoFactor="Tasa" TasaOCuota="1.600000" Importe="360000"/>
        </cfdi:Traslados>
    </cfdi:Impuestos>
</cfdi:Comprobante>
XML;
    return $xmlstr;
    }

    private function generarCadena($xsltFile, $xmlFile)
    {
        //$process = new Process('xalan -xsl ' . $xsltFile . ' -in ' . $xmlFile);
        $process = new Process('xsltproc ' . $xsltFile . ' ' . $xmlFile);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        } else { 
            return $process->getOutput();
        }
    }
    
    /**
     * 
     * @return string
     */
    private function timbrar()
    {
        $this->cfdi = $this->generarLayout($this->rfcEmisor);
        $params = array('emisorRFC' => $this->rfcEmisor,'UserID' => $this->userId,'UserPass' => $this->password);
        $opciones = array();

        /**
        * Establecer el valor a true, si desea que el Web services genere el CBB en
        * formato PNG correspondiente.
        * Nota: Utilizar está opción deshabilita 'generarPDF'
        */     
        $opciones['generarCBB'] = TRUE;

        /**
        * Establecer el valor a true, si desea que el Web services genere la
        * representación impresa del XML en formato PDF.
        * Nota: Utilizar está opción deshabilita 'generarCBB'
        */
        $opciones['generarPDF'] = TRUE;

        /**
        * Establecer el valor a true, si desea que el servicio genere un archivo de
        * texto simple con los datos del Nodo: TimbreFiscalDigital
        */
        $opciones['generarTXT'] = TRUE;
        
        $cliente = new FacturacionModerna($this->timbradoURL, $params, $this->debugLevel);
        if($cliente->timbrar($this->cfdi, $opciones)){

            //Almacenanos en la raíz del proyecto los archivos generados.
            
            $comprobante = $this->get('kernel')->getProjectDir() . "/public/comprobantes/" . $cliente->UUID;

            if($cliente->xml){
              $response = "XML almacenado correctamente en $comprobante.xml\n";        
              \file_put_contents($comprobante . ".xml", $cliente->xml);
            }
            if(isset($cliente->pdf)){
              $response = "PDF almacenado correctamente en $comprobante.pdf\n";
              \file_put_contents($comprobante . ".pdf", $cliente->pdf);
            }
            if(isset($cliente->png)){
              $response =  "CBB en formato PNG almacenado correctamente en $comprobante.png\n";
              \file_put_contents($comprobante . ".png", $cliente->png);
            }

            $response .= "Timbrado exitoso\n";

          }else{
            $response =  "[" . $cliente->ultimoCodigoError."] - " . $cliente->ultimoError . "\n";
          }
          return $response;
    }
    
    private function generarLayout($rfcEmisor)
    {

        $fecha_actual = substr( date('c'), 0, 19);

        /*
          Puedes encontrar más ejemplos y documentación sobre estos archivos aquí. (Factura, Nota de Crédito, Recibo de Nómina y más...)
          Link: https://github.com/facturacionmoderna/Comprobantes
          Nota: Si deseas información adicional contactanos en www.facturacionmoderna.com
        */

        $cfdi = <<<LAYOUT
[ComprobanteFiscalDigital]

; Definición del layout INI para Comprobantes Fiscales por Internet Versión 3.3
; Nota: Los comentarios empiezan con ; 
; Elaboró: Facturación Moderna
; Contacto: wsoporte@facturacionmoderna.com

Version=3.3
Serie=A
Folio=02
Fecha=$fecha_actual
FormaPago=03
NoCertificado=20001000000300022762
CondicionesDePago=CONTADO
SubTotal=1850
Descuento=175.00
Moneda=MXN
Total=1943.00
TipoDeComprobante=I
MetodoPago=PUE
LugarExpedicion=68050

[DatosAdicionales]
tipoDocumento=FACTURA
observaciones=Observaciones al documento versión 3.3
platillaPDF=clasic

[Emisor]
Rfc=$rfcEmisor
Nombre=FACTURACION MODERNA SA DE CV
RegimenFiscal=601

[Receptor]
Rfc=XAXX010101000
Nombre=PUBLICO EN GENERAL
UsoCFDI=G01


[Concepto#1]
ClaveProdServ=01010101
NoIdentificacion=AULOG001
Cantidad=5
ClaveUnidad=H87
Unidad=Pieza
Descripcion=Aurriculares USB Logitech
ValorUnitario=350.00
Importe=1750.00
Descuento=175.00

Impuestos.Traslados.Base=[1575.00]
Impuestos.Traslados.Impuesto=[002]
Impuestos.Traslados.TipoFactor=[Tasa]
Impuestos.Traslados.TasaOCuota=[0.160000]
Impuestos.Traslados.Importe=[252.00]

[Concepto#2]
ClaveProdServ=43201800
NoIdentificacion=USB
Cantidad=1
ClaveUnidad=H87
Unidad=Pieza
Descripcion=Memoria USB 32gb marca Kingston
ValorUnitario=100.00
Importe=100.00

Impuestos.Traslados.Base=[100.00]
Impuestos.Traslados.Impuesto=[002]
Impuestos.Traslados.TipoFactor=[Tasa]
Impuestos.Traslados.TasaOCuota=[0.160000]
Impuestos.Traslados.Importe=[16.00]

[Traslados]
TotalImpuestosTrasladados=268.00
Impuesto=[002]
TipoFactor=[Tasa]
TasaOCuota=[0.160000]
Importe=[268.00]

LAYOUT;
    return $cfdi;
    }
}