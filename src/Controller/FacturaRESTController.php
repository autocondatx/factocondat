<?php

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

namespace App\Controller;

/**
 * Description of FacturaController
 *
 * @author erparom
 * @Route("/")
 */
class FacturaRESTController extends FOSRestController
{
    /**
      * @Route("/timbrar", name="timbrar_rest")
      */
    public function timbrarRestAction($foo, $bar)
    {
        return $this->view(array($foo, $bar), 200); 
    }
}
