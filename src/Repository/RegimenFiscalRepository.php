<?php

namespace App\Repository;

use App\Entity\RegimenFiscal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RegimenFiscal|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegimenFiscal|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegimenFiscal[]    findAll()
 * @method RegimenFiscal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegimenFiscalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RegimenFiscal::class);
    }

//    /**
//     * @return RegimenFiscal[] Returns an array of RegimenFiscal objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegimenFiscal
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
