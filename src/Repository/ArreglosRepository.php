<?php

namespace App\Repository;

use App\Entity\Arreglos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Arreglos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Arreglos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Arreglos[]    findAll()
 * @method Arreglos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArreglosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Arreglos::class);
    }

//    /**
//     * @return Arreglos[] Returns an array of Arreglos objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Arreglos
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
