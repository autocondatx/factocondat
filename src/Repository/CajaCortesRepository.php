<?php

namespace App\Repository;

use App\Entity\CajaCortes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CajaCortes|null find($id, $lockMode = null, $lockVersion = null)
 * @method CajaCortes|null findOneBy(array $criteria, array $orderBy = null)
 * @method CajaCortes[]    findAll()
 * @method CajaCortes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CajaCortesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CajaCortes::class);
    }

//    /**
//     * @return CajaCortes[] Returns an array of CajaCortes objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CajaCortes
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
