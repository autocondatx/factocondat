<?php

namespace App\Repository;

use App\Entity\EstatusCuentas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EstatusCuentas|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstatusCuentas|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstatusCuentas[]    findAll()
 * @method EstatusCuentas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstatusCuentasRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EstatusCuentas::class);
    }

//    /**
//     * @return EstatusCuentas[] Returns an array of EstatusCuentas objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EstatusCuentas
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
