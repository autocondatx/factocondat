<?php

namespace App\Repository;

use App\Entity\TiposIngreso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TiposIngreso|null find($id, $lockMode = null, $lockVersion = null)
 * @method TiposIngreso|null findOneBy(array $criteria, array $orderBy = null)
 * @method TiposIngreso[]    findAll()
 * @method TiposIngreso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TiposIngresoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TiposIngreso::class);
    }

//    /**
//     * @return TiposIngreso[] Returns an array of TiposIngreso objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TiposIngreso
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
