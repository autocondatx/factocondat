<?php

namespace App\Repository;

use App\Entity\TiposMovimientosCaja;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TiposMovimientosCaja|null find($id, $lockMode = null, $lockVersion = null)
 * @method TiposMovimientosCaja|null findOneBy(array $criteria, array $orderBy = null)
 * @method TiposMovimientosCaja[]    findAll()
 * @method TiposMovimientosCaja[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TiposMovimientosCajaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TiposMovimientosCaja::class);
    }

//    /**
//     * @return TiposMovimientosCaja[] Returns an array of TiposMovimientosCaja objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TiposMovimientosCaja
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
