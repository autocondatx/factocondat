<?php

namespace App\Repository;

use App\Entity\TiposPrenda;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TiposPrenda|null find($id, $lockMode = null, $lockVersion = null)
 * @method TiposPrenda|null findOneBy(array $criteria, array $orderBy = null)
 * @method TiposPrenda[]    findAll()
 * @method TiposPrenda[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TiposPrendaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TiposPrenda::class);
    }

//    /**
//     * @return TiposPrenda[] Returns an array of TiposPrenda objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TiposPrenda
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
