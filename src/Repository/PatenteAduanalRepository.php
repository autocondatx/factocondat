<?php

namespace App\Repository;

use App\Entity\PatenteAduanal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PatenteAduanal|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatenteAduanal|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatenteAduanal[]    findAll()
 * @method PatenteAduanal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatenteAduanalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PatenteAduanal::class);
    }

//    /**
//     * @return PatenteAduanal[] Returns an array of PatenteAduanal objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PatenteAduanal
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
