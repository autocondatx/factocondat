<?php

namespace App\Repository;

use App\Entity\TiposArreglo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TiposArreglo|null find($id, $lockMode = null, $lockVersion = null)
 * @method TiposArreglo|null findOneBy(array $criteria, array $orderBy = null)
 * @method TiposArreglo[]    findAll()
 * @method TiposArreglo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TiposArregloRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TiposArreglo::class);
    }

//    /**
//     * @return TiposArreglo[] Returns an array of TiposArreglo objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TiposArreglo
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
