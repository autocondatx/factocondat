<?php

namespace App\Repository;

use App\Entity\Franquicia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Franquicia|null find($id, $lockMode = null, $lockVersion = null)
 * @method Franquicia|null findOneBy(array $criteria, array $orderBy = null)
 * @method Franquicia[]    findAll()
 * @method Franquicia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FranquiciaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Franquicia::class);
    }

//    /**
//     * @return Franquicia[] Returns an array of Franquicia objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Franquicia
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
