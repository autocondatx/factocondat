<?php

namespace App\Repository;

use App\Entity\ClaveProdServ;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ClaveProdServ|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClaveProdServ|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClaveProdServ[]    findAll()
 * @method ClaveProdServ[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClaveProdServRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ClaveProdServ::class);
    }

//    /**
//     * @return ClaveProdServ[] Returns an array of ClaveProdServ objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClaveProdServ
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
