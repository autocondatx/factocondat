<?php

namespace App\Repository;

use App\Entity\EstatusFacturas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EstatusFacturas|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstatusFacturas|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstatusFacturas[]    findAll()
 * @method EstatusFacturas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstatusFacturasRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EstatusFacturas::class);
    }

//    /**
//     * @return EstatusFacturas[] Returns an array of EstatusFacturas objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EstatusFacturas
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
