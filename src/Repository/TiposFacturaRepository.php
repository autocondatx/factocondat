<?php

namespace App\Repository;

use App\Entity\TiposFactura;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TiposFactura|null find($id, $lockMode = null, $lockVersion = null)
 * @method TiposFactura|null findOneBy(array $criteria, array $orderBy = null)
 * @method TiposFactura[]    findAll()
 * @method TiposFactura[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TiposFacturaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TiposFactura::class);
    }

//    /**
//     * @return TiposFactura[] Returns an array of TiposFactura objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TiposFactura
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
