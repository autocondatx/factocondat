<?php

namespace App\Repository;

use App\Entity\ClaveUnidad;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ClaveUnidad|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClaveUnidad|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClaveUnidad[]    findAll()
 * @method ClaveUnidad[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClaveUnidadRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ClaveUnidad::class);
    }

//    /**
//     * @return ClaveUnidad[] Returns an array of ClaveUnidad objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClaveUnidad
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
