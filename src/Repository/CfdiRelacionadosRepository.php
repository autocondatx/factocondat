<?php

namespace App\Repository;

use App\Entity\CfdiRelacionados;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CfdiRelacionados|null find($id, $lockMode = null, $lockVersion = null)
 * @method CfdiRelacionados|null findOneBy(array $criteria, array $orderBy = null)
 * @method CfdiRelacionados[]    findAll()
 * @method CfdiRelacionados[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CfdiRelacionadosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CfdiRelacionados::class);
    }

//    /**
//     * @return CfdiRelacionados[] Returns an array of CfdiRelacionados objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CfdiRelacionados
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
