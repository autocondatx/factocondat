<?php

namespace App\Repository;

use App\Entity\EstatusPedido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EstatusPedido|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstatusPedido|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstatusPedido[]    findAll()
 * @method EstatusPedido[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstatusPedidoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EstatusPedido::class);
    }

//    /**
//     * @return EstatusPedido[] Returns an array of EstatusPedido objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EstatusPedido
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
