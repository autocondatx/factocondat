<?php

namespace App\Repository;

use App\Entity\Egreso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Egreso|null find($id, $lockMode = null, $lockVersion = null)
 * @method Egreso|null findOneBy(array $criteria, array $orderBy = null)
 * @method Egreso[]    findAll()
 * @method Egreso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EgresoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Egreso::class);
    }

//    /**
//     * @return Egreso[] Returns an array of Egreso objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Egreso
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
