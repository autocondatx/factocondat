<?php

namespace App\Repository;

use App\Entity\Emisor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Emisor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Emisor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Emisor[]    findAll()
 * @method Emisor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmisorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Emisor::class);
    }

//    /**
//     * @return Emisor[] Returns an array of Emisor objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Emisor
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
