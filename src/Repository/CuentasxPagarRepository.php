<?php

namespace App\Repository;

use App\Entity\CuentasxPagar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CuentasxPagar|null find($id, $lockMode = null, $lockVersion = null)
 * @method CuentasxPagar|null findOneBy(array $criteria, array $orderBy = null)
 * @method CuentasxPagar[]    findAll()
 * @method CuentasxPagar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CuentasxPagarRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CuentasxPagar::class);
    }

//    /**
//     * @return CuentasxPagar[] Returns an array of CuentasxPagar objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CuentasxPagar
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
