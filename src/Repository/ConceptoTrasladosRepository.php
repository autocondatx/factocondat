<?php

namespace App\Repository;

use App\Entity\ConceptoTraslados;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ConceptoTraslados|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConceptoTraslados|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConceptoTraslados[]    findAll()
 * @method ConceptoTraslados[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConceptoTrasladosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ConceptoTraslados::class);
    }

//    /**
//     * @return ConceptoTraslados[] Returns an array of ConceptoTraslados objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConceptoTraslados
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
