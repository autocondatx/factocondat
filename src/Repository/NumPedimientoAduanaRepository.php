<?php

namespace App\Repository;

use App\Entity\NumPedimientoAduana;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NumPedimientoAduana|null find($id, $lockMode = null, $lockVersion = null)
 * @method NumPedimientoAduana|null findOneBy(array $criteria, array $orderBy = null)
 * @method NumPedimientoAduana[]    findAll()
 * @method NumPedimientoAduana[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NumPedimientoAduanaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NumPedimientoAduana::class);
    }

//    /**
//     * @return NumPedimientoAduana[] Returns an array of NumPedimientoAduana objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NumPedimientoAduana
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
