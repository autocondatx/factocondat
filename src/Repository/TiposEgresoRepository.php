<?php

namespace App\Repository;

use App\Entity\TiposEgreso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TiposEgreso|null find($id, $lockMode = null, $lockVersion = null)
 * @method TiposEgreso|null findOneBy(array $criteria, array $orderBy = null)
 * @method TiposEgreso[]    findAll()
 * @method TiposEgreso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TiposEgresoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TiposEgreso::class);
    }

//    /**
//     * @return TiposEgreso[] Returns an array of TiposEgreso objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TiposEgreso
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
