<?php

namespace App\Repository;

use App\Entity\TipoFactor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TipoFactor|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoFactor|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoFactor[]    findAll()
 * @method TipoFactor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoFactorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TipoFactor::class);
    }

//    /**
//     * @return TipoFactor[] Returns an array of TipoFactor objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoFactor
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
