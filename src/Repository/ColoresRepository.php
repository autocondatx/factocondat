<?php

namespace App\Repository;

use App\Entity\Colores;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Colores|null find($id, $lockMode = null, $lockVersion = null)
 * @method Colores|null findOneBy(array $criteria, array $orderBy = null)
 * @method Colores[]    findAll()
 * @method Colores[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ColoresRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Colores::class);
    }

//    /**
//     * @return Colores[] Returns an array of Colores objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Colores
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
