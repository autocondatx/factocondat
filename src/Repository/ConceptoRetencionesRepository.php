<?php

namespace App\Repository;

use App\Entity\ConceptoRetenciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ConceptoRetenciones|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConceptoRetenciones|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConceptoRetenciones[]    findAll()
 * @method ConceptoRetenciones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConceptoRetencionesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ConceptoRetenciones::class);
    }

//    /**
//     * @return ConceptoRetenciones[] Returns an array of ConceptoRetenciones objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConceptoRetenciones
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
