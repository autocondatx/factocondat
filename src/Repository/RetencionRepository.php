<?php

namespace App\Repository;

use App\Entity\Retencion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Retencion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Retencion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Retencion[]    findAll()
 * @method Retencion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RetencionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Retencion::class);
    }

//    /**
//     * @return Retencion[] Returns an array of Retencion objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Retencion
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
