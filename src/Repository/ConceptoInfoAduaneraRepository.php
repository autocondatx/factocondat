<?php

namespace App\Repository;

use App\Entity\ConceptoInfoAduanera;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ConceptoInfoAduanera|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConceptoInfoAduanera|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConceptoInfoAduanera[]    findAll()
 * @method ConceptoInfoAduanera[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConceptoInfoAduaneraRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ConceptoInfoAduanera::class);
    }

//    /**
//     * @return ConceptoInfoAduanera[] Returns an array of ConceptoInfoAduanera objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConceptoInfoAduanera
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
