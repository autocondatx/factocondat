<?php

namespace App\Repository;

use App\Entity\CuentaBancoMovimientos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CuentaBancoMovimientos|null find($id, $lockMode = null, $lockVersion = null)
 * @method CuentaBancoMovimientos|null findOneBy(array $criteria, array $orderBy = null)
 * @method CuentaBancoMovimientos[]    findAll()
 * @method CuentaBancoMovimientos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CuentaBancoMovimientosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CuentaBancoMovimientos::class);
    }

//    /**
//     * @return CuentaBancoMovimientos[] Returns an array of CuentaBancoMovimientos objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CuentaBancoMovimientos
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
