<?php

namespace App\Repository;

use App\Entity\TasaOCuota;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TasaOCuota|null find($id, $lockMode = null, $lockVersion = null)
 * @method TasaOCuota|null findOneBy(array $criteria, array $orderBy = null)
 * @method TasaOCuota[]    findAll()
 * @method TasaOCuota[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TasaOCuotaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TasaOCuota::class);
    }

//    /**
//     * @return TasaOCuota[] Returns an array of TasaOCuota objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TasaOCuota
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
