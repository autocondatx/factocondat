<?php

namespace App\Repository;

use App\Entity\CajaMovimientos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CajaMovimientos|null find($id, $lockMode = null, $lockVersion = null)
 * @method CajaMovimientos|null findOneBy(array $criteria, array $orderBy = null)
 * @method CajaMovimientos[]    findAll()
 * @method CajaMovimientos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CajaMovimientosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CajaMovimientos::class);
    }

//    /**
//     * @return CajaMovimientos[] Returns an array of CajaMovimientos objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CajaMovimientos
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
