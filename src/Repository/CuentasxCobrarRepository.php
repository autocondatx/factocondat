<?php

namespace App\Repository;

use App\Entity\CuentasxCobrar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CuentasxCobrar|null find($id, $lockMode = null, $lockVersion = null)
 * @method CuentasxCobrar|null findOneBy(array $criteria, array $orderBy = null)
 * @method CuentasxCobrar[]    findAll()
 * @method CuentasxCobrar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CuentasxCobrarRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CuentasxCobrar::class);
    }

//    /**
//     * @return CuentasxCobrar[] Returns an array of CuentasxCobrar objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CuentasxCobrar
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
