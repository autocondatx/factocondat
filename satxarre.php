<?php
//
// +---------------------------------------------------------------------------+
// | satxarre.php : Genera arreglo asociativo en base a la factura del ERP     |
// +---------------------------------------------------------------------------+
// | Copyright (c) 2005  Fabrica de Jabon la Corona, SA de CV                  |
// +---------------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or             |
// | modify it under the terms of the GNU General Public License               |
// | as published by the Free Software Foundation; either version 2            |
// | of the License, or (at your option) any later version.                    |
// |                                                                           |
// | This program is distributed in the hope that it will be useful,           |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// | GNU General Public License for more details.                              |
// |                                                                           |
// | You should have received a copy of the GNU General Public License         |
// | along with this program; if not, write to the Free Software               |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA|
// +---------------------------------------------------------------------------+
// | Autor: Fernando Ortiz <fortiz@lacorona.com.mx>                            |
// +---------------------------------------------------------------------------+
// | 06/abr/2016 Complemento Comercio Exterior (cce)                           |
// +---------------------------------------------------------------------------+
//
function satxarre($nufa,$addenda="",$version="3.2") {
// {{{ carga librerias requeridas para obtener los datos
require_once("dbi/clfactur.class.php");  // Es el registro maestro de la factura
require_once("dbi/clflinea.class.php");  // Regisro por partida de la factura
require_once("dbi/clcadena.class.php");  // Cadenas
require_once("dbi/clclient.class.php");  // Clientes
require_once("dbi/clpedido.class.php");  // Pedido
require_once("lib/cn_envio_bodega.php");  // Lista de clientes 'nuestros'
require_once("lib/cn_envio_export.php");  // Bodegas nuestras extranjero
require_once("lib/cn_simplificado.php");  // RFC generico pero IVA desglosado
require_once("lib/getrefe.php");  
require_once("lib/getcoes.php");  
require_once("lib/fmtfech.php");          // Cambia de dmy a ymd
global $conn;                            // Conexion adodb a la base de datos
// }}}
// {{{ Inicialice varibles / contadores globales
//error_reporting(E_ALL);
error_reporting(E_ALL ^ E_NOTICE);
/* Lectura de tablas de la base de datos */
//            Facturas
$fact = new Clfactur($conn,$nufa,'renglon');
//            Lineas de la factura, un renglon por producto facturado
$fali = new Clflinea($conn, $nufa);
//           Archivo maestro de clientes
$clie = new Clclient($conn,(int)$fact->row['factnucl'],'renglon');
//           Catalogo de cadenas (Domicilio fiscal de Soriana, walmart, etc)
$cade = new Clcadena($conn,(int)$clie->row['clienuca'],'renglon');
//           Pedidos 
$pedi = new Clpedido($conn,(int)$fact->row['factnupe'],'renglon');
$arr = array();
// }}}
// {{{ Encabezados generales 
$arr['folio'] = substr($nufa,4);
$arr['fecha'] = str_replace(array('-',' ',':'),'',fix_fdoc_arre($fact->row["factfdoc"]));
$arr['serie'] = substr($nufa,0,4);
//  
// +---------------------------------------------------------------------------+
// | $arr['noAprobacion'] 
// | $arr['anoAprobacion'] 
// | $arr['noCertificado'] 
$arr['version'] = $version;
if (strcmp(fmtfech(substr($fact->row["factfdoc"],0,10)),'2013-12-21')<=0) {
   $arr['version'] = "2.2";
}
require "lib/satxfoli.inc.php"; 
// +---------------------------------------------------------------------------+
//  
$arr['subTotal'] = $fact->row["factneto"]+$fact->row['factnet2'];  // Antes de impuestos
//          Se desglosa iva a partir del 01/11/2013   Javier Samperio
if (strcmp(fmtfech(substr($fact->row["factfdoc"],0,10)),'2013-10-31')<=0) {
    if ($fact->row["factrfca"]=="XAXX010101000" && !cn_simplificado($fact->row["factnucl"])) {
       $arr['subTotal'] = (double)$fact->row["factimto"];  // = al Total no desglosa iva 
    }
}
//$arr['subTotal'] = $fact->row["factimpa"]+$fact->row['factimp2'];  // Antes de impuestos
//$arr['descuento'] = $fact->row["factdecl"]+$fact->row['factdec2'];  // prueba
$arr['total'] = (double)$fact->row["factimto"];  // Despues de impuestos
$nuca = $fact->row["factnuca"]; 
$cadetipa = $cade->row["cadetipa"]; 
$cadecuen = $cade->row["cadecuen"]; 
$clietipa = $clie->row["clietipa"]; 
$cliecuen = $clie->row["cliecuen"]; 
$peditipa = $pedi->row["peditipa"]; 
$pedicuen = $pedi->row["pedicuen"]; 
if ($clietipa>0) {
    $cadetipa = $clietipa;
    $cadecuen = $cliecuen;
}
if ($peditipa>0) {
    $cadetipa = $peditipa;
    $cadecuen = $pedicuen;
}

if ($cadetipa==1 OR $cadetipa==98 OR $cadetipa==99) {
    $cadecuen = "";
}

if (!$cadetipa) {
    $arr['metodoDePago'] = ($version=="3.3") ? "99" : "NA";
    $arr['NumCtaPago'] = "";
    $aux = "NA";
} else {
    $arr['metodoDePago'] = trim(getrefe("cte",6,"CLIETIPA",$cadetipa));
    $arr['NumCtaPago'] = $cadecuen;
    $aux = str_pad($cadetipa,2,'0',STR_PAD_LEFT);
}
$arr['NumCtaPago'] = str_replace('.',' ',$arr['NumCtaPago'] );
if (strcmp(fmtfech(substr($fact->row["factfdoc"],0,10)),'2016-06-06')>=0) {
    $arr['metodoDePago'] = $aux;
}

if ( cn_envio_bodega($fact->row["factnucl"]) && 
     !cn_envio_export($fact->row["factnucl"])) {
         #
         # Trasalado entre nuestras bodegas
         #
    $arr['tipoDeComprobante'] = ($version=="3.3") ? "T" :"traslado";
    $orig = trim($conn->GetOne("Select bodenomb from clbodega WHERE bodenubo = ".$fact->row["factnubo"]));
    $dest = trim($conn->GetOne("Select bodenomb from clbodega WHERE bodenucl = ".$fact->row["factnucl"]));
    $arr['formaDePago'] = ($version=="3.3") ? "" :"ESTE COMPROBANTE SE EXPIDE PARA TRANSPORTAR MERCANCIAS DE NUESTRA PROPIEDAD DE BODEGA $orig A BODEGA $dest";
} else {
         #
         # Todas las demas facturas
         #
    $arr['tipoDeComprobante'] = ($version=="3.3") ? "I" : "ingreso";
    $arr['formaDePago'] = ($version=="3.3") ? "PUE" :"PAGO EN UNA SOLA EXHIBICION";
}
// FOM que ya se solo se quedara solo uno 25/05/2016 msanchez
//if ($nuca == 39 || $nuca == 939)
//    $arr['formaDePago'] = "EL PAGO DE ESTA FACTURA (CONTRAPRESTACION) SE EFECTUARA EN UNA SOLA EXHIBICION, SI POR ALGUNA RAZON NO FUERA ASI, LO PAGARAN EN LAS PARCIALIDADES RESPECTIVAS";
$arr['condicionesDePago']="NO APLICA";
$arr['TipoCambio']="1";
$arr['Moneda']="MXN";
$arr['LugarExpedicion']=trim($fact->row["factecol"])." ".trim($fact->row["factepob"]);
if ($addenda=="cce" || $version=="3.3") {
    $codp = trim($conn->GetOne("Select bodecodl from clbodega WHERE bodenubo = ".$fact->row["factnubo"]));
   $arr['LugarExpedicion']=$codp;
}

$arr['Emisor']['nombre'] = "FABRICA DE JABON LA CORONA SA DE CV";
$arr['Emisor']['rfc'] = "FJC780315E91";
$arr['Emisor']['Regimen'] = "REGIMEN GENERAL DE LEY DE PERSONAS MORALES";
if ($addenda=="cce" || $version=="3.3") {
   $arr['Emisor']['Regimen'] = "601";
}
$arr['Emisor']['ExpedidoEn']['calle'] = "CARLOS B ZETINA";
$arr['Emisor']['ExpedidoEn']['noExterior'] = "80";
$arr['Emisor']['ExpedidoEn']['noInterior'] = "";
$arr['Emisor']['ExpedidoEn']['localidad'] = "";
$arr['Emisor']['ExpedidoEn']['colonia'] = "PARQUE INDUSTRIAL XALOSTOC";
$arr['Emisor']['ExpedidoEn']['municipio'] = "ECATEPEC DE MORELOS";
$arr['Emisor']['ExpedidoEn']['estado'] = "ESTADO DE MEXICO";
$arr['Emisor']['ExpedidoEn']['pais'] = "MEXICO";
$arr['Emisor']['ExpedidoEn']['codigoPostal'] = "55348";

if ($addenda=="cce") {
    $arr['Emisor']['ExpedidoEn']['colonia'] = "3738";
    $arr['Emisor']['ExpedidoEn']['localidad'] = "5";
    $arr['Emisor']['ExpedidoEn']['municipio'] = "033";
    $arr['Emisor']['ExpedidoEn']['estado'] = "MEX";
    $arr['Emisor']['ExpedidoEn']['pais'] = "MEX";
}

$arr['Receptor']['nombre'] = $fact->row["factnopr"];
// Usa tabla ISO8859-2
//if ($nuca == 29)
    //$arr['Receptor']['nombre'] = "ORGANIZACI".chr(211)."N SAHUAYO, S.A. DE C.V.";
$arr['Receptor']['rfc'] = $fact->row["factrfca"];
$arr['Receptor']['UsoCFDI'] = "P01";

// 22/dic/2013  Sefactura valida que el Digito verificador sea digito ....
// 20/ene/2014  Se confirma con Javier Samperio tambien puede ser A
if (strlen(trim($arr['Receptor']['rfc'])) == 13) {
    // Solo personas fisicas
   $ultima = substr($arr['Receptor']['rfc'],-1);
   if ($ultima < "0" || $ultima > "9") {
       if ($ultima != "A") {
           $arr['Receptor']['rfc'] = substr_replace($arr['Receptor']['rfc'],"0",-1);
       }
   }
} 

if (strlen(trim($cade->row['cadecall']))) {
    $a_edo = $cade->row["cadecoes"];
    $a_calle = $cade->row["cadecall"];
    $a_noExterior = $cade->row["cadenext"];
    $a_noInterior = $cade->row["cadenint"];
    $a_colonia = $cade->row["cadecolo"];
    $a_localidad = $cade->getPueb("cadecoes","cademuni","cadepobl");
    $a_municipio = $cade->getMuni("cadecoes","cademuni");
    $a_codigoPostal = str_pad($cade->row["cadecodp"],5,'0',STR_PAD_LEFT);
} else {
    $a_edo = $clie->row["cliecoes"];
    $a_calle = $fact->row["factcdir"];
    $a_noExterior = $fact->row["factnext"];
    $a_noInterior = $fact->row["factnint"];
    $a_colonia = $fact->row["factccol"];
    // 25/10/2011 Para que lo tome de donde debe
    // $a_localidad = $fact->row["factnpue"];
    // $a_municipio = $fact->row["factnpue"];
    $a_localidad = $clie->getPueb("cliecoes","cliemuni","cliepueb");
    $a_municipio = $clie->getMuni("cliecoes","cliemuni");
    $a_codigoPostal = str_pad($fact->row["factcodp"],5,'0',STR_PAD_LEFT);
}

if ($nuca == 29) { // SAHUAYO
    $a_edo = $clie->row["cliecoes"];
    $a_municipio = $clie->getPueb("cliecoes","cliemuni","cliepueb");
    $a_colonia = satxarre_fix($fact->row["factccol"],35);
    $a_calle = satxarre_fix($a_calle,35);
}
//$edo = trim(getrefe("cte",6,"CLIECOES",$a_edo));
$edo = trim(getcoes($a_edo));
$pais = ($a_edo>40) ? $edo : "MEXICO";
$arr['Receptor']['Domicilio']['calle'] = $a_calle;
$arr['Receptor']['Domicilio']['noExterior'] = $a_noExterior;
$arr['Receptor']['Domicilio']['noInterior'] = $a_noInterior;
$arr['Receptor']['Domicilio']['colonia'] = $a_colonia;
$arr['Receptor']['Domicilio']['localidad'] = $a_localidad;
$arr['Receptor']['Domicilio']['municipio'] = $a_municipio;
$arr['Receptor']['Domicilio']['estado'] = $edo;
$arr['Receptor']['Domicilio']['pais'] = $pais;
$arr['Receptor']['Domicilio']['codigoPostal'] = $a_codigoPostal;

if ($addenda=="cce") {
    $a_edoe = $fact->row["factedoe"];
    $paissat = trim($conn->GetOne("SELECT estashcp FROM clestado
                                     WHERE estacoes = $a_edoe"));
    $arr['Receptor']['Domicilio']['pais'] = $paissat;
    $a_mune = $fact->row["factmune"];
    $estasat = trim($conn->GetOne("SELECT munishcp FROM cltamuni
          WHERE municoes = $a_edoe and munimuni = $a_mune"));
    $arr['Receptor']['Domicilio']['estado'] = $estasat;
}

// }}}
// {{{  Datos para el esquema de detalllista solo si hace falta
if ($addenda=="detallista") {
    // +--------------------------------------------------------------------+
    // | Lee los datos de npec, fpec, gln solo si quieren complemento       |
    // +--------------------------------------------------------------------+
    //
    $arr['Complemento']['npec'] = $fact->row['factnpec'];
    $arr['Complemento']['fpec'] = $fact->row['factfpec'];
    if ($nuca == 115) {
        $arr['Complemento']['gln'] = str_pad($fact->row['factnucl'],13,'0',STR_PAD_LEFT);
        $arr['Complemento']['gln2'] = str_pad("0000000001867",13,'0',STR_PAD_LEFT);
        $arr['Complemento']['proveedor'] = str_pad("01867",5,'0',STR_PAD_LEFT);
    }
    if ($nuca == 87) {
        $arr['Complemento']['gln'] = str_pad("7504000225003",13,'0',STR_PAD_LEFT);
        $arr['Complemento']['gln2'] = str_pad("7505000065005",13,'0',STR_PAD_LEFT);
        $arr['Complemento']['ship'] = str_pad("7504000225027",13,'0',STR_PAD_LEFT);
        $arr['Complemento']['proveedor'] = str_pad("84",2,'0',STR_PAD_LEFT);
    }
}
// }}}
// {{{  Datos para complemento Comercio Exterior (cce)
if ($addenda=="cce") {
    $inco = $clie->row["cliebanc"]; 
    $incoterm = $conn->getone("select substr(docurefe,0,10) from document
        where docunuli = $inco and docucvsi = 6 and docutire = 'INCOTERM'");
    $desc = trim(substr($desc,0,10));
    $arr['Complemento']['TipoOperacion'] = "2";
    $arr['Complemento']['ClaveDePedimento'] = "A1";
    $arr['Complemento']['CertificadoOrigen'] = "0";
    $arr['Complemento']['Incoterm'] = trim($incoterm); // Leerlo de cada cliente !!!!!
    $arr['Complemento']['Subdivision'] = "0";
    $tipo = $fact->row["facttipc"];
    $tipoUSD = $fact->row["facttipc"];
    $tipoUSD = round($tipoUSD,2);
    $arr['Complemento']['TipoCambioUSD'] = $tipoUSD;
    $taxi = trim($clie->row['clietaxi']);
    $taxi = str_replace(array("-","/"," "),"",$taxi);
    $arr['Complemento']['Receptor']['NumRegIdTrib'] = $taxi;
}
// }}}
// {{{  Diconsa
// +--------------------------------------------------------------------+
// | Lee los datos de almacen solo si es diconsa                        |
// +--------------------------------------------------------------------+
//
if ($addenda=="diconsa") {
    $arr['diconsa']['proveedor'] = 217;
    $arr['diconsa']['almacen'] = trim($clie->row['cliesucu']);
    $arr['diconsa']['negociacion'] = trim($fact->row['factnpec']);
//    $arr['diconsa']['pedido'] = 0;
    $arr['diconsa']['pedido'] = satxarre_nume($fact->row['factobs4']);
    if (!$arr['diconsa']['pedido']) {
        $arr['diconsa']['pedido'] = 1;
    }
    if ($arr['diconsa']['pedido']==0) {
        $arr['diconsa']['pedido'] = 1;
    }
}
// }}}
// {{{  IMSS
// +--------------------------------------------------------------------+
// | Lee los datos de almacen solo si es imss                           |
// +--------------------------------------------------------------------+
//
if ($addenda=="imss") {
    $sucu = (int)$clie->row["cliezovo"];
    $sucu = str_pad($sucu, 5, "0", STR_PAD_LEFT);
    $arr['imss']['proveedor'] = "0000029727";
    $arr['imss']['delegacion'] = $sucu;
    $arr['imss']['conceptodocumento'] = "ORIGINAL";
    $arr['imss']['documento'] = "FACTURA";
    $arr['imss']['moneda'] = "MXN";
    $arr['imss']['transaccion'] = "FACTURACION";
    $arr['imss']['cambio'] = "1.00";
    $arr['imss']['concepto'] = "TN";
    #$arr['imss']['pedido'] = trim($fact->row['factnpec']);
    #$arr['imss']['recepcion'] = trim($conn->getone("select corenpec from clcorevi where coredocu = '$nufa'"));
    #$arr['imss']['serie'] = "N/A";
}
// }}}
// {{{   Para cada linea/partida de la factura (para cada producto
// +--------------------------------------------------------------------+
// | AHora si procesa la ocurrencia de productos de la factura          |
// +--------------------------------------------------------------------+
//
if ($addenda=="cce") { // cce
    $totaldolares = 0;
}
$arr['Impuestos'] = array();
for ($i=0; $i<sizeof($fali->faliprod); $i++) {
    $cant = (int)$fali->faliunif[$i];
    $unid = get_unidad($fali->faliprod[$i]);
    $impo = (double)$fali->falineto[$i];
    if (strcmp(fmtfech(substr($fact->row["factfdoc"],0,10)),'2013-10-31')<=0) {
      if ($fact->row["factrfca"]=="XAXX010101000" && !cn_simplificado($fact->row["factnucl"])) {
            $impo = (double)$fali->faliimto[$i];
        }
    }
    if ($arr['serie']=='FIVA' && $fact->row["factrfca"]!="XAXX010101000")
        $arr['Conceptos'][$i+1]['descripcion'] = $fact->row['factobs1'].' '.
                                                 $fact->row['factobs2'].' '.
                                                 $fact->row['factobs3'];
    else
        $arr['Conceptos'][$i+1]['descripcion'] = trim($fali->falideco[$i]);
    $producto = $fali->faliprod[$i];
    if ($fali->faliprod[$i]==99025 || $fali->faliprod[$i]==99000 ||
        $fali->faliprod[$i]==99013 || $fali->faliprod[$i]==99014 ||
        $fali->faliprod[$i]==99015 || $fali->faliprod[$i]==99017 ||
        $fali->faliprod[$i]==99041) 
        $refe1 = trim(getrefe("cte",200,$fact->row["factnupe"],1));
        $refe2 = trim(getrefe("cte",200,$fact->row["factnupe"],2));
        $refe3 = trim(getrefe("cte",200,$fact->row["factnupe"],3));
        $arr['Conceptos'][$i+1]['descripcion'] = trim($fali->falideco[$i]).' '.
                                                 $refe1.' '.
                                                 $refe2.' '.
                                                 $refe3;
   if (strcmp(fmtfech(substr($fact->row["factfdoc"],0,10)),'2014-04-04')>=0) {
//     echo "paso fecha";
       if($cade->row["cadetico"]==1 || $fact->row['facttine'] == "O" || 
          $fact->row['facttine'] == "X" ) {
//            echo "paso tine";
              $mlinea = $conn->getone("SELECT prodline FROM clproduc 
                                        WHERE prodprod = $producto");
              $mfami = $conn->getone("SELECT prodfami FROM clproduc 
                                       WHERE prodprod = $producto");
// echo "mlinea=".$mlinea, " mfami=".$mfami;
          IF ($mlinea <> 2) {
              $w_d_fami = getrefe("cte",6,"MARCAEX2",$mfami);
              IF (stripos($fali->falideco[$i],"*S/FOSFATO*")) {
                 $w_d_fami = getrefe("cte",6,"MARCAEX3",$mfami);
              }
              IF ($fali->faliprod[$i] == 40450) $w_d_fami = "ROMA LAUNDRY DETERGENT, BAGS WITH :";
              IF ($fali->faliprod[$i] == 71025) $w_d_fami = "COOKING OIL 1-2-3 ";
          }
// echo "w_d_fami=".$w_d_fami;
          $arr['Conceptos'][$i+1]['descripcion'] = trim($w_d_fami).' '.trim($fali->falideco[$i]);
       }
    }

    $arr['Conceptos'][$i+1]['cantidad'] = $cant;
    $arr['Conceptos'][$i+1]['unidad']=$unid;
    $arr['Conceptos'][$i+1]['noIdentificacion']=$fali->falicbar[$i];
    $si = (strcmp(fmtfech(substr($fact->row["factfdoc"],0,10)),'2012-01-09')>=0)?'t':'f';
    $decimales = ($si=="t") ? 6 : 2;
    if ($cant == 0) {
        $prun = (double)$impo;
    }else{
        $prun = round((double)$impo / (double)$cant,$decimales);
    }
    $arr['Conceptos'][$i+1]['valorUnitario'] = $prun;
    $arr['Conceptos'][$i+1]['importe'] = $impo;
    if ($version=="3.3") {
        $c_prodserv = $conn->getone("SELECT prodpsat FROM clproduc 
                                        WHERE prodprod = $producto");
        if (trim($c_prodserv)=="") $c_prodserv="01010101";
        $arr['Conceptos'][$i+1]['ClaveProdServ'] = $c_prodserv;
        $arr['Conceptos'][$i+1]['ClaveUnidad'] = get_unidad_33($producto);
        $TasaOCuota = number_format(($fali->falipoim[$i]/100),6);
        // echo "TasaOCuota=$TasaOCuota poim=".$fali->falipoim[$i];
        $arr['Conceptos'][$i+1]['TasaOCuota'] = $TasaOCuota;
        $arr['Conceptos'][$i+1]['impuesto'] = $fali->faliimpu[$i];
        if (!array_key_exists($TasaOCuota,$arr['Impuestos'])) $arr['Impuestos'][$TasaOCuota] = 0;
        $arr['Impuestos'][$TasaOCuota] += $fali->faliimpu[$i];
    }
    if ($addenda=="detallista") {
        $arr['Conceptos'][$i+1]['poim'] = $fali->falipoim[$i];
        $arr['Conceptos'][$i+1]['impu'] = $fali->faliimpu[$i];
        $arr['Conceptos'][$i+1]['gtin'] = trim($fali->falicbar[$i]);
        if ($arr['Conceptos'][$i+1]['gtin']=="") {
            $arr['Conceptos'][$i+1]['gtin'] = "X";
        }
        $arr['Conceptos'][$i+1]['prun'] = $prun;
        $arr['Conceptos'][$i+1]['neto'] = $fali->falineto[$i];
        if ($nuca == 87) {
	    $producto = $fali->faliprod[$i];
	    $nubo = $fact->row["factnubo"];
            $prodheb = trim($conn->getone("select promrefe from clpromoc
	      where promprod = $producto and promnuca = $nuca and promnubo = $nubo"));
            $arr['Conceptos'][$i+1]['hebprod'] = $prodheb;
        }
    } // complemento Comercio Exterior (cce)
    if ($addenda=="cce") { // cce
        $peso = $conn->getone("SELECT prodpeso FROM clproduc 
                                WHERE prodprod = $producto");
        $unid_cce = get_unidad_cce($fali->faliprod[$i]);
        $arr['Conceptos'][$i+1]['UnidadAduana']=$unid_cce;
	$cant = $cant * $peso;
        $arr['Conceptos'][$i+1]['CantidadAduana']=$cant;
        $id = str_pad($fali->faliprod[$i],5,'0',STR_PAD_LEFT);
        $arr['Conceptos'][$i+1]['noIdentificacion']=$id;
        $arr['Conceptos'][$i+1]['NoIdentificacion']=$id;
        $frac = get_fraccion($fali->faliprod[$i]);
        $arr['Conceptos'][$i+1]['FraccionArancelaria'] = $frac;
        $dolares = round($impo / $tipo,2);
        $precio = round($dolares / $cant,2);
        if ($fali->faliprod[$i]==99991 || $fali->faliprod[$i]==99986) {
            $arr['Conceptos'][$i+1]['UnidadAduana']="99";
            $arr['Conceptos'][$i+1]['FraccionArancelaria'] = "";
            $arr['Conceptos'][$i+1]['CantidadAduana']=1;
            $precio = 0.00;
            $dolares = 0.00;
        }
        $arr['Conceptos'][$i+1]['ValorUnitarioAduana'] = number_format($precio,2,".","");;
        $arr['Conceptos'][$i+1]['ValorDolares'] = number_format($dolares,2,".","");;
        $totaldolares += $dolares;
    } // complemento Comercio Exterior (cce)
}
if ($addenda=="cce") { // cce
    $arr['Complemento']['TotalUSD'] = number_format($totaldolares,2,".","");;
}
// }}}
// {{{ Finaliza el arreglo
$arr['Traslados']['impuesto'] = "IVA";
$arr['Traslados']['tasa'] = $fact->row["factpoim"];
$arr['Traslados']['importe'] = (double)$fact->row["factimpu"];
if (strcmp(fmtfech(substr($fact->row["factfdoc"],0,10)),'2013-10-31')<=0) {
if ($fact->row["factrfca"]=="XAXX010101000" && !cn_simplificado($fact->row["factnucl"])) {
   $arr['Traslados']['tasa'] = "0";
   $arr['Traslados']['importe'] = "0.00";
}
}
return($arr);
}
// }}}
// {{{ get_unidad : lee la descripcion de undiad del document del producto cajas, pzas, etc
function get_unidad($prod) {
global $conn;
$desc = $conn->getone("select substr(docurefe,0,20) from document, clproduc where prodprod=$prod and docunuli = produven and docucvsi = 6 and docutire = 'PRODUVEN'");
$desc = trim(substr($desc,0,20));
if ($desc=="") $desc="Caja";
return($desc);
}
// }}}
// {{{ get_unidad_cce : lee la clave de unidad en base a sat:cce:c_Unidad
function get_unidad_cce($prod) {
global $conn;
$refe = $conn->getone("select docurefe from document, clproduc where prodprod=$prod and docunuli = produven and docucvsi = 6 and docutire = 'PRODUVEN'");
$clave = trim(substr($refe,60,2));
if ($clave=="") $clave="99";
return($clave);
}
// }}}
// {{{ get_unidad_33 : lee la clave de unidad en base a sat:cce:c_Unidad
function get_unidad_33($prod) {
global $conn;
$refe = $conn->getone("select docurefe from document, clproduc where prodprod=$prod and docunuli = produven and docucvsi = 6 and docutire = 'PRODUVEN'");
$clave = trim(substr($refe,65,3));
if ($clave=="") $clave="ZZ";
return($clave);
}
// {{{ get_fraccion : Lee fraccion arancelaria
function get_fraccion($prod) {
global $conn;
$fracc = $conn->getone("select prodaran from clproduc where prodprod=$prod");
$fracc = trim(str_replace(array("-","."," "),"",$fracc));
return($fracc);
}
// }}}
// {{{ fix_fdoc_arre : Cuando el timestamp viene dd/mm/yyyy lo convierte a yyyy-mm-dd
function fix_fdoc_arre($fdoc) {
    if (strpos($fdoc,"/")!==FALSE) { // tiene diagonales viene dd/mm/yyyy hh:mm
        list($f,$h)=explode(" ",$fdoc);
        list($d,$m,$y)=explode("/",$f);
        $fdoc = "$y-$m-$d $h";
    }
    return ($fdoc);
}
// }}}
// {{{ Convierte el caracter especial a char(209)
function satxarre_fix($str,$largo=-1) {
    $tmp = trim($str);
    $tmp = str_replace(array("}","{"),chr(209),$tmp);
    if ($largo>0) $tmp = substr($tmp,0,$largo-1);
    return ($tmp);
                }
// }}}
// {{{ Selecciona solo numeros
function satxarre_nume($str,$largo=-1) {
    $tmp = trim($str);
    $nume = "";
    for ($i=0;$i<strlen($tmp);$i++) {
        $a = $tmp{$i};
        if ($a == "0" || $a == "1" || $a == "2" || $a == "3" || $a == "4" ||
            $a == "5" || $a == "6" || $a == "7" || $a == "8" || $a == "9") {
                $nume = $nume.$a;
        }
    }
    $tmp = trim($nume);
    return ($tmp);
}